{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fprof-auto #-}
module T1J
  ( T1J()
  , initT1J
  , updateT1J
  , evaluateT1J
  , criticalPoints
  , showT1JASCII
  ) where

import Control.Lens
import Control.Monad
import Control.Monad.Loops
import Control.Monad.ST
import Data.List (foldl')
import Data.Vector.Unboxed (Vector, (!))
import Safe
import SC.Types

import Coord
import Game
import Geometry
import Links
import Protocol
import Score

import qualified Data.Vector.Unboxed as Vector
import qualified Data.Vector.Unboxed.Mutable as MVector

import qualified PointSet

cMALUS_1 :: Int
cMALUS_1 = 2 * cMULT + 1

cMALUS_2 :: Int
cMALUS_2 = cMULT + 3

cMULT :: Int
cMULT = 10

cBLOCKED_FIELD_VAL :: Int
cBLOCKED_FIELD_VAL = 99

cBLOCKED_FIELD_VAL_MULT :: Int
cBLOCKED_FIELD_VAL_MULT = cBLOCKED_FIELD_VAL * cMULT

data T1J = T1J
  { t1jFather :: !(Vector Point)
  , t1jValue :: !(Vector Int)
  , t1jRelevant :: !(Vector Point)
  } deriving (Eq, Ord, Show)

pConnectionBlocked :: Point
pConnectionBlocked = Point (-1) (-1)

pPointBlocked :: Point
pPointBlocked = Point (-1) (-2)

pSwampBlocked :: Point
pSwampBlocked = Point (-1) (-3)

fatherBlockLinks :: Point -> Direction -> [Link]
fatherBlockLinks point fatherDir =
  [ Link point' arrow
  | arrow <- calmArrows fatherDir
  , let point' = point & inDirection (arrowMajor arrow) (-1), pointValid point'
  ]

findPossibleFather :: PlayerColor -> Direction -> TwixtState -> Point -> Point -> Point
findPossibleFather player fatherDir TwixtState{..} point def
  | point^.axisCoord (fatherDir^.dirAxis) == 0 = point
  | isConnectionBlocked = pConnectionBlocked
  | (father : _) <- checkOwnLinks = father
  | twixtHasSwamp point' = pSwampBlocked
  | twixtHasPeg (oppositePlayerColor player) point' = pPointBlocked
  | twixtHasPeg player point' = point'
  | otherwise = def
 where
  isConnectionBlocked = any twixtHasLink (fatherBlockLinks point fatherDir)
  point' = point & inDirection fatherDir 1
  checkOwnLinks =
    [ target
    | link <- map (Link point) (directionArrows fatherDir)
    , let target = linkTarget link, pointValid target
    , twixtHasPeg player target && twixtLinkable link
    , not $ twixtHasLink link
    ]

blockedByRace :: PlayerColor -> Bool -> TwixtState -> Point -> Arrow -> Bool
blockedByRace player enemyNext TwixtState{..} point direction = pointValid blockPeg
  && twixtHasPeg (oppositePlayerColor player) blockPeg
  && maybe True (== oppositePlayerColor player) (cardinalSide direction blockPeg)
  && (  twixtHasLink blockLink
     || enemyNext
     && twixtHasLink (Link blockPeg (reverseArrow direction))
     && twixtLinkable blockLink
     )
 where
  blockPeg = inDirection (arrowMajor direction) (-1) point
  blockLink = Link blockPeg direction

pegBorderReachable :: PlayerColor -> Bool -> TwixtState -> Direction -> Point -> Point -> Bool
pegBorderReachable player enemyNext TwixtState{..} dir pegPoint relPoint
  =  dCol == 0 || not (enemyNext && dCol >= 2)
  && (abs (rCol - maxLine) < 6 || blockNear)
 where
  axis' = orthogonalAxis (dir^.dirAxis)
  pCol = pegPoint^.axisCoord axis'
  rCol = relPoint^.axisCoord axis'
  dCol = abs (pCol - rCol)
  maxLine = if dir^.dirPositive then 23 else 0
  enemyPegAbove d = any (twixtHasPeg (oppositePlayerColor player))
    [ relPoint & axisCoord axis' +~ d & inDirection dir x | x <- [1..4] ]
  blockNear
    | dCol == 1        = enemyPegAbove 0
    | pCol == rCol - 2 = enemyPegAbove (-1)
    | pCol == rCol + 2 = enemyPegAbove 1
    | otherwise = False

checkRaceOk :: PlayerColor -> Bool -> TwixtState -> Direction -> Point -> Bool
checkRaceOk player enemyNext state@TwixtState{..} dir point = notBlocked
  && not (any (blockedByRace player enemyNext state point) (directionArrows dir))
 where
  notBlocked = not enemyNext
    || all (checkPeg peg1Arrows) oneBlocks && all (checkPeg peg2Arrows) twoBlocks
  axis' = orthogonalAxis (dir^.dirAxis)
  mkPoint (dmajor, dminor) = point & inDirection dir dmajor & axisCoord axis' +~ dminor
  oneBlocks = filter pointValid $ map mkPoint [(2, 0), (1, 0), (1, 1), (1, -1)]
  twoBlocks = filter pointValid $ map mkPoint [(3, 0), (4, 0)]
  peg1Arrows = directionArrows (reverseDirection dir) ++ calmArrows dir
  peg2Arrows = steepArrows (reverseDirection dir)
  checkPeg arrows p = not $ twixtHasPeg (oppositePlayerColor player) p
    && any twixtLinkable (map (Link p) arrows)

pointDistance :: Axis -> Point -> Vector Point -> Maybe (Int, Point)
pointDistance axis point fathers
  | blocked = Nothing
  | otherwise = Just (f $ fromEnum (point^.axisCoord axis - father^.axisCoord axis) * cMULT, father)
 where
  axis' = orthogonalAxis axis
  father = fathers ! fromEnum point
  blocked = father^.row == -1
  f | abs (father^.axisCoord axis' - point^.axisCoord axis') == 1 = (+1)
    | abs (father^.axisCoord axis' - point^.axisCoord axis') == 2 = (+3)
    | otherwise = id

computeValues
  :: PlayerColor -> Bool -> Direction -> TwixtState -> Vector Point
  -> (Vector Point, Vector Int)
computeValues player enemyNext fatherDirection state@TwixtState{..} fathers = runST $ do
  relevants <- MVector.replicate 576 0
  values <- MVector.replicate 576 cBLOCKED_FIELD_VAL_MULT
  forM_ orderedPoints $ \point ->
    if twixtHasPeg player point
      then do
        let (point', malus') = checkFatherLinks fatherDirection state fathers point
            relevant = fathers ! fromEnum point'
            accept = relevant^.axisCoord axis' /= 0 ||
                 pegBorderReachable player enemyNext state fatherDirection point relevant
              && checkRaceOk player enemyNext state fatherDirection point
        val' <- (malus' +) <$> dist point' values
        val <- MVector.read values (fromEnum point)
        when (val' < val && accept) $ do
          MVector.write relevants (fromEnum point) relevant
          forM_ (map fromEnum $ twixtConnected point) $ MVector.write values ?? val'
      else MVector.write values (fromEnum point) 0
  (,) <$> Vector.unsafeFreeze relevants <*> Vector.unsafeFreeze values
 where
  axis = fatherDirection^.dirAxis
  axis' = orthogonalAxis axis
  orderedPoints = case axis of
    Row -> [Point r c | r <- [0..23], c <- [0..23]]
    Col -> [Point r c | c <- [0..23], r <- [0..23]]

  dist point values = case pointDistance axis point fathers of
    Nothing -> pure cBLOCKED_FIELD_VAL_MULT
    Just (d, p) -> (d + ) <$> MVector.read values (fromEnum p)

checkFatherLinks :: Direction -> TwixtState -> Vector Point -> Point -> (Point, Int)
checkFatherLinks fatherDirection TwixtState{..} fathers point = headDef (point, 0)
  [ (target, computeMalus arrow)
  | father == pPointBlocked || father == pSwampBlocked
  , arrow <- fatherArrows
  , let conn = Link point arrow; target = linkTarget conn
  , pointValid target && not (twixtHasSwamp target) && twixtLinkable conn
  ]
 where
  axis = fatherDirection^.dirAxis
  father = fathers ! fromEnum point
  fatherArrows = directionArrows fatherDirection

  computeMalus arrow
    | arrow^.to arrowMajor.dirAxis == axis = cMALUS_1
    | otherwise = cMALUS_2

initT1J :: PlayerColor -> TwixtState -> T1J
initT1J player state@TwixtState{..} = T1J{..}
 where
  next = twixtNextPlayer
  axis = playerAxis player
  player' = oppositePlayerColor player
  enemyNext = next == player'
  scanDirection = Direction axis True
  fatherDirection = reverseDirection scanDirection

  t1jFather = Vector.constructN 576 $ \v ->
    let point = toEnum $ Vector.length v
        point' = inDirection fatherDirection 1 point
    in findPossibleFather player fatherDirection state point (v ! fromEnum point')

  (t1jRelevant, t1jValue) =
    computeValues player enemyNext fatherDirection state t1jFather

updateT1J :: PlayerColor -> TwixtState -> Move -> T1J -> T1J
updateT1J selfPlayer state@TwixtState{..} (Move point) T1J{..} = T1J
  { t1jFather = t1jFather', t1jValue = t1jValue', t1jRelevant = t1jRelevant' }
 where
  movePlayer = twixtNextPlayer
  leftConnected = any twixtHasLink (newLinks False)
  rightConnected = any twixtHasLink (newLinks True)
  newLinks = map (Link point) . steepArrows . Direction axis'
  columnsToUpdate = [0,-2,-1,1,2] ++ [-3 | leftConnected] ++ [3 | rightConnected]
  t1jFather' = Vector.modify (\vec -> mapM_ (updateColumn vec) columnsToUpdate) t1jFather
  pointsToUpdate dcol = takeWhile (validPointCorner . fst) $
    [ (point'', x)
    | x <- [-1..], let point'' = point' & inDirection scanDirection x
    , x > 0 || validPointCorner point''
    ]
   where
    point' = point & axisCoord axis' +~ dcol
  validPointCorner (Point x y) = x < 24 && y < 24 && x >= 0 && y >= 0
  updateColumn vec dcol = andM $ pointsToUpdate dcol <&> \(p, drow) -> do
    let point' = inDirection fatherDirection 1 p
    def <- if validPointCorner point' then MVector.read vec . fromEnum $ point' else return p
    let father = findPossibleFather selfPlayer fatherDirection state p def
    previous <- MVector.read vec (fromEnum p)
    MVector.write vec (fromEnum p) father
    return (previous /= father || drow < 4)

  enemyNext = movePlayer == selfPlayer
  axis = playerAxis selfPlayer
  axis' = orthogonalAxis axis
  scanDirection = Direction axis True
  fatherDirection = reverseDirection scanDirection
  (t1jRelevant', t1jValue') =
    computeValues selfPlayer enemyNext fatherDirection state t1jFather'

evaluateT1J :: PlayerColor -> TwixtState -> T1J -> (Score, [Point])
evaluateT1J player state@TwixtState{..} T1J{..}
  = foldl' evaluatePoint (maxBound, []) lastRowPoints & over _1 (negate . toEnum)
 where
  next = twixtNextPlayer
  axis = playerAxis player
  axis' = orthogonalAxis axis
  scanDirection = Direction axis True
  enemyNext = player == next
  lastRowPoints = [0 & axisCoord axis .~ 23 & axisCoord axis' .~ x | x <- [0..23]]
  evaluatePoint r@(!bestScore, !crits) point
    | twixtHasPeg player point && pointValue <= bestScore = (pointValue, crit pointValue point)
    | father^.axisCoord axis > 0 && distVal <= bestScore && accept = (distVal, crit distVal father)
    | otherwise = r
   where
    pointValue = t1jValue ! fromEnum point
    crit v p = if v `quot` 10 < bestScore `quot` 10 then [p] else p : crits
    father = t1jFather ! fromEnum point
    accept = pegBorderReachable player enemyNext state scanDirection point father
      && checkRaceOk player enemyNext state scanDirection father
    distVal = maybe cBLOCKED_FIELD_VAL_MULT mkDistVal $ pointDistance axis point t1jFather
    mkDistVal (x, p) = x + t1jValue ! fromEnum p

criticalPoints :: PlayerColor -> TwixtState -> T1J -> [(Point, Bool)]
criticalPoints player state@TwixtState{..} t1j@T1J{..} = concatMap go startingPoints
 where
  axis = playerAxis player
  (_, startingPoints) = evaluateT1J player state t1j
  go start = addStart $ case ends of
    (end:_) -> addEnd end . go $ t1jRelevant ! fromEnum end
    [] -> []
   where
    addStart
      | start^.axisCoord axis < 23 && twixtHasPeg player start = ((start,True) : )
      | otherwise = id
    addEnd e
      | e^.axisCoord axis > 0 = ((e,False) : )
      | otherwise = id
    ends = filter (\x -> t1jRelevant ! fromEnum x /= 0) . twixtConnected $ start

readT1J :: String -> [Move]
readT1J = map parseMove . dropWhile ('#' `elem`) . lines
 where
  parseMove (c:r) = Move $ Point (read r - 1) (fromIntegral $ fromEnum c - fromEnum 'A')
  parseMove _ = error "invalid t1j file"

_loadT1J :: PlayerColor -> PlayerColor -> FilePath -> IO T1J
_loadT1J player next file = do
  moves <- readT1J <$> readFile file
  print moves
  let
    applyMoves (Move point : ms) b (p1, p2, links) = applyMoves ms (not b) (p1', p2', links')
     where
      p1' = if b then PointSet.insert point p1 else p1
      p2' = if not b then PointSet.insert point p2 else p2
      p = if b then p1' else p2'
      links' = updateLinks (`PointSet.member` p) (Move point) links
    applyMoves _ _ x = x
    (ps1, ps2, conns) = applyMoves moves True (mempty, mempty, emptyLinks)
  return . initT1J player $ TwixtState
    { twixtHasSwamp = const False
    , twixtHasPeg = \p point -> PointSet.member point (if p == PlayerRed then ps1 else ps2)
    , twixtHasLink = hasLink ?? conns
    , twixtLinkable = linkable ?? conns
    , twixtConnected = findConnected ?? conns
    , twixtNextPlayer = next
    }


showT1JASCII :: T1J -> String
showT1JASCII T1J{..} = unlines $ concat
  [ "Fathers:" : showVector showPoint t1jFather
  , "Values: " : showVector show t1jValue
  , "Relevant: " : showVector showPoint t1jRelevant
  ]
 where
  showD x = (if x < 0 then ('-' :) else id) $ if abs x < 10 then '0' : show (abs x) else show (abs x)
  showVector f vector = [0..23] <&> \r ->
    concat . (showD r :) $ [0..23] <&> \c ->
      let p = Point r c in "\t" ++ f (vector ! fromEnum p)
  showPoint (Point r c) = showD r ++ ":" ++ showD c
