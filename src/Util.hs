module Util
  ( bitsetToList
  , makeUnderscoreLenses
  ) where

import Control.Lens
import Data.Bits
import GHC.Exts
import Language.Haskell.TH

-- | Return the positions of all set bits in the given integer.
-- The indicies are returned in increasing order.
--
-- >>> bitsetToList (9 :: Int)
-- [0,3]
--
-- >>> bitsetToList (0 :: Int)
-- []
bitsetToList :: (FiniteBits a, Num a) => a -> [Int]
bitsetToList bits = build $ \c z ->
  let
    go 0 = z
    go n = let i = countTrailingZeros n in c i (go (n `clearBit` i))
  in go bits
{-# INLINE bitsetToList #-}

-- | Generate lenses for all field accessors of the given data types. The lenses will
-- be named after the field accessor, but prefixed with an underscore.
makeUnderscoreLenses :: Name -> DecsQ
makeUnderscoreLenses = makeLensesWith $ lensRules
  & lensField .~ prefixUnderscore
 where
  prefixUnderscore _ _ field = [TopName (mkName $ '_' : nameBase field)]
