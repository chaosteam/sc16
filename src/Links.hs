{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BangPatterns #-}
-- | A data structure that efficiently stores which links there are on a twixt board.
module Links
  ( Links()
  , initLinks, emptyLinks, updateLinks
  , addLink, addLinkIfPossible, unsafeAddLink, linkable, pointLinks, hasLink
  , findConnected
  ) where

import Control.Lens
import Control.Monad.ST
import Data.Bits
import Data.Foldable (foldlM)
import Data.List (foldl')
import Data.Maybe
import Data.Hashable
import Data.Vector.Unboxed (Vector, (!))
import Data.Word
import GHC.Exts (build)
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen

import qualified Data.Vector.Unboxed as Vector
import qualified Data.Vector.Unboxed.Mutable as MVector

import Coord
import Game
import Protocol
--------------------------------------------------------------------------------

{- $setup
>>> import Data.List
>>> import Control.Monad
-}

-- | A data type to store links on a twixt board.
newtype Links = Links
  { -- | The following field stores the links for each point on the board in a vector.
    --
    -- For each point, we store a 8 bit value: each bit is set iff there is a connection in
    -- the direction of the arrow with the enum value of the bit's position.
    _links :: Vector Word8
  } deriving (Eq, Ord, Show)
makeLenses ''Links

instance Arbitrary Links where
  arbitrary = foldl' (flip addLinkIfPossible) emptyLinks <$> listOf arbitrary

instance Hashable Links where
  hashWithSalt salt (Links conns) = hashWithSalt salt $ Vector.toList conns

-- | Convert a connection to the format used in the @_connections@ vector.
linkValues :: Link -> (Int, Word8)
linkValues (Link point arrow) = (fromEnum point, bit $ fromEnum arrow)

-- | Convert a bit value to the list of arrows it represents.
bitsToArrows :: Word8 -> [Arrow]
bitsToArrows b = filter (testBit b . fromEnum) allArrows

-- | Create a new links structure representing the state specified by the server state.
initLinks :: ServerState -> Links
initLinks ServerState{serverLinks} = emptyLinks & links %~ (Vector.unsafeAccum (.|.) ?? conns)
 where
  conns = [linkValues (Link a (a `arrowTo` b)) | (_,x,y) <- serverLinks, (a,b) <- [(x,y), (y,x)]]

-- | Given a function that returns True iff the given point has a peg by the player who made the
-- current move, update the links for this move.
updateLinks :: (Point -> Bool) -> Move -> Links -> Links
updateLinks f (Move point) = foldl' go ?? allArrows
 where
  go conns arrow = fromMaybe conns $ do
    conn <- makeLink f point arrow
    addLink conn conns

-- | Create a new, empty links structure.
emptyLinks :: Links
emptyLinks = Links (Vector.replicate 576 0)

-- | @addLink point arrow connections@ adds a connection from @point@ going in the direction
-- of @arrow@ to the links in @connections@ and returns the new collection.
--
-- This function returns 'Nothing' if the connection is not possible because it would intersect
-- with existing collections.
--
-- prop> mfilter (not . hasLink c) (addLink c conns) == Nothing
-- prop> addLink c cs == addLink (reverseLink c) cs
addLink :: Link -> Links -> Maybe Links
addLink conn conns
  | linkable conn conns = Just $ unsafeAddLink conn conns
  | otherwise = Nothing

-- | Add a connection if possible (doesn't intersect with any other connections), else return
-- the board unchanged.
addLinkIfPossible :: Link -> Links -> Links
addLinkIfPossible conn conns = fromMaybe conns (addLink conn conns)

-- | Like 'connect', but without the safety check that ensures that the connection doesn't
-- intersect with any other connection.
--
-- /Note/: Be careful if you use this function, if the invariant is not satisified, it may corrupt
-- the collection.
--
-- prop> hasLink conn (unsafeAddLink conn emptyLinks)
-- prop> hasLink (reverseLink conn) (unsafeAddLink conn emptyLinks)
unsafeAddLink :: Link -> Links -> Links
unsafeAddLink conn = over links $ Vector.accum (.|.) ??
  [linkValues conn, linkValues (reverseLink conn)]

-- | Returns True if a the specific connection is possible (not blocked by other connections).
linkable :: Link -> Links -> Bool
linkable conn (Links conns) = foldr checkBlock True (linkBlocks conn)
 where
  checkBlock l a = a && not ((conns ! fromEnum (linkSource l)) `testBit` fromEnum (linkDirection l))

-- | Return the links of the given point. The returned arrow will point away from the
-- given point to the other point of the connection.
--
-- >>> let links0 = unsafeAddLink (Link (Point 3 3) (makeArrow dUp False)) emptyLinks
-- >>> let links1 = unsafeAddLink (Link (Point 3 3) (makeArrow dDown False)) links0
-- >>> let links2 = unsafeAddLink (Link (Point 5 4) (makeArrow dDown False)) links1
-- >>> sort $ pointLinks (Point 3 3) links2
-- [aUpLeft,aDownRight]
pointLinks :: Point -> Links -> [Arrow]
pointLinks point (Links conns) = bitsToArrows (conns ! fromEnum point)

-- | Check whether the given connection exists.
hasLink :: Link -> Links -> Bool
hasLink (Link point arrow) (Links conns)
  = (conns ! fromEnum point) `testBit` fromEnum arrow

-- | Get all the points transitively reachable through links starting at a given point.
--
-- The points are returned in an unspecified order. Each point is only returned once, even if it
-- is reachable through multiple paths. The starting point is always returned.
--
-- >>> let links0 = unsafeAddLink (Link (Point 3 3) (makeArrow dUp False)) emptyLinks
-- >>> let links1 = unsafeAddLink (Link (Point 3 3) (makeArrow dDown False)) links0
-- >>> let links2 = unsafeAddLink (Link (Point 5 4) (makeArrow dDown False)) links1
-- >>> sort $ findConnected (Point 3 3) links2
-- [Point 1 2,Point 3 3,Point 5 4,Point 7 5]
-- >>> sort $ findConnected (Point 3 3) links0
-- [Point 1 2,Point 3 3]
--
-- >>> findConnected (Point 3 3) emptyLinks
-- [Point 3 3]
findConnected :: Point -> Links -> [Point]
findConnected start (Links conns) = build $ \c z -> runST $ do
  v <- Vector.thaw conns
  let
    go !a point = do
      let a' = c point a
      b <- MVector.unsafeRead v (fromEnum point)
      MVector.unsafeWrite v (fromEnum point) 0
      if b == 0
        then return a
        else foldlM go a' (map (`applyArrow` point) (bitsToArrows b))
  b <- MVector.unsafeRead v (fromEnum start)
  if b == 0
    then return (c start z)
    else go z start
