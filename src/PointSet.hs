{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- | A specialized, very efficient Set capable of storing Points.
module PointSet
  ( PointSet()
  , size
  , member
  , singleton
  , fromList
  , toList
  , foldMap
  , minValue
  , unsafeMinValue
  , maxValue
  , unsafeMaxValue
  , insert
  , delete
  , difference
  , intersection
  , inversion
  , connectedComponents
  , drawASCII
  , printASCII
  , printASCIISingle
  ) where

import Control.DeepSeq
import Control.Lens
import Data.Bits
import Data.Bits.Lens
import Data.Bool
import Data.DoubleWord
import Data.DoubleWord.TH
import Data.Hashable
import Data.List (foldl', intercalate)
import Data.Maybe
import Data.Typeable (Typeable)
import Data.Data (Data(..))
import Data.Semigroup
import Data.Serialize
import Data.Word
import GHC.Exts (build)
import GHC.Generics (Generic(..))
import Test.QuickCheck (Arbitrary(..))

import Prelude hiding (foldMap)
import qualified Data.Foldable as Foldable

import Coord

mkUnpackedDoubleWord "Word512" ''Word256 "Int512" ''Int256 ''Word256
  [''Typeable, ''Data, ''Generic]
mkUnpackedDoubleWord "Word576" ''Word512 "Int576" ''Int512 ''Word64
  [''Typeable, ''Data, ''Generic]

-- | A PointSet is like a 'Set Point', but more optimized.
-- Internally, it's stored as a bitset, so that most operations can be done
-- in O(1) time.
--
-- We need to be able to store @24 * 24 - 4 = 572@ positions at least.
-- The nearest viable bitset size for this is @512 + 64 = 576@, so we use a
-- 'Word576' for the bitset.
newtype PointSet = PointSet { unPointSet :: Word576 }
  deriving (Eq, Ord, Generic, Hashable)

instance Show PointSet where
  showsPrec d ps = showParen (d >= 11) $ showString "PointSet.fromList " . showsPrec 11 (toList ps)

-- | Access the underlying bit field
_PointSet :: Iso' PointSet Word576
_PointSet = iso unPointSet PointSet
{-# INLINE _PointSet #-}

instance NFData PointSet where
  rnf !_ = ()

instance Serialize PointSet where
  put (PointSet w) = put (toInteger w)
  get = PointSet . fromInteger <$> get

instance Arbitrary PointSet where
  arbitrary = (`difference` boardCorners) . fromList <$> arbitrary

-- | A point set that contains only the corners of the board,
-- onto which one may not place a peg.
boardCorners :: PointSet
boardCorners = fromList $ Point <$> [0,24] <*> [0,24]

-- | The monoid instance for point sets takes the union of the two sets.
instance Monoid PointSet where
  mempty = PointSet zeroBits
  mappend (PointSet a) (PointSet b) = PointSet (a .|. b)

instance Semigroup PointSet where
  (<>) = mappend

type instance Index PointSet = Point
type instance IxValue PointSet = ()

instance Contains PointSet where
  contains point = _PointSet.bitAt (fromEnum point)
  {-# INLINE contains #-}

instance Ixed PointSet where
  ix = ixAt
  {-# INLINE ix #-}

instance At PointSet where
  at point f = contains point $ fmap isJust . f . bool Nothing (Just ())
  {-# INLINE at #-}

-- | Insert a point into the set.
-- Has no effect if the point is already in the set.
--
-- prop> member p (insert p s)
-- prop> insert p (insert p s) == insert p s
-- prop> delete p (insert p s) == delete p s
insert :: Point -> PointSet -> PointSet
insert p (PointSet s) = PointSet $ s `setBit` fromEnum p

-- | Create a PointSet containing exactly the specified points.
--
-- prop> fromList (toList ps) == ps
fromList :: [Point] -> PointSet
fromList = foldl' (flip insert) mempty

-- | Enumerate all members of the point set.
--
-- /Warning/: This function might be slow! Use with care.
toList :: PointSet -> [Point]
toList p = filter (flip member p) allPoints
{-# INLINE toList #-}

-- | Map a function over the point set, aggregating the results using the Monoid instance.
foldMap :: Monoid m => (Point -> m) -> PointSet -> m
foldMap f = Foldable.foldMap f . toList

-- | Retrieve the minimum point (by its Enum value) in this point set.
-- Returns @Nothing@ if the set is empty.
minValue :: PointSet -> Maybe Point
minValue (PointSet 0) = Nothing
minValue (PointSet s) = Just $ firstPoint s

-- | Like 'minValue', but assuming that the set is non-empty.
--
-- /Attention/: This function might return a wrong result if the set
-- is actually empty. Only use it when you're sure that there's at least
-- one entry in the set.
unsafeMinValue :: PointSet -> Point
unsafeMinValue (PointSet s) = firstPoint s

-- | Retrieve the maximum point (by its Enum value) in this point set.
-- Returns @Nothing@ if the set is empty.
maxValue :: PointSet -> Maybe Point
maxValue ps = if ps == mempty then Nothing else Just (unsafeMinValue ps)

-- | Like 'maxValue', but assuming that the set is non-empty.
--
-- /Attention/: This function might return a wrong result if the set
-- is actually empty. Only use it when you're sure that there's at least
-- one entry in the set.
unsafeMaxValue :: PointSet -> Point
unsafeMaxValue (PointSet s) = toEnum $ finiteBitSize s - 1 - countLeadingZeros s

-- | Delete a point from the set.
-- Has no effect if the point isn't a member of the set.
--
-- prop> not (member p (delete p s))
-- prop> delete p (delete p s) == delete p s
-- prop> insert p (delete p s) == insert p s
delete :: Point -> PointSet -> PointSet
delete p (PointSet s) = PointSet $ s `clearBit` fromEnum p

-- | Return the number of points in the set.
--
-- prop> size mempty == 0
-- prop> size (insert p s) >= size s
-- prop> size (delete p s) <= size s
size :: PointSet -> Int
size (PointSet s) = popCount s

-- | Build a set that contains all the points in the first argument except
-- those which are also members of the second set.
--
-- prop> difference s mempty == s
-- prop> not (member p (s `difference` singleton p))
difference :: PointSet -> PointSet -> PointSet
difference (PointSet a) (PointSet b) = PointSet $ a .&. complement b

-- | Compute the intersection of two sets, i.e. the set of points which are
-- in both of the passed sets.
intersection :: PointSet -> PointSet -> PointSet
intersection (PointSet a) (PointSet b) = PointSet $ a .&. b

-- | Invert a point set. This builds a set with all valid points except
-- those which are members of the argument.
--
-- prop> inversion (inversion s) == s
-- prop> not (member p (inversion (insert p s)))
inversion :: PointSet -> PointSet
inversion (PointSet a) = PointSet (complement a) `difference` boardCorners

-- | Return True iff the given point is a member of the specified set.
member :: Point -> PointSet -> Bool
member p (PointSet s) = s `testBit` fromEnum p
{-# INLINE member #-}

-- | Create a set with only a single member.
singleton :: Point -> PointSet
singleton p = insert p mempty

-- | Split the point set, such that each returned point set only
-- contains points that are neighbours of other points in this
-- point set. A neighbour is any point that is either above, below,
-- left or right another point.
--
-- prop> mconcat (connectedComponents ps) == ps
connectedComponents :: PointSet -> [PointSet]
connectedComponents ps = build $ \c z ->
  let
    go 0 = z
    go b = let (r, b') = findNeighbours (pure b) (firstPoint b) in c r (go b')
  in go (unPointSet ps)

-- | Find the smallest point in the given point set. This function
-- assumes that a smallest point does exist.
firstPoint :: Word576 -> Point
firstPoint = toEnum . countTrailingZeros

-- | Split all points that are connected through neighbourship
-- to the given point.
findNeighbours :: (PointSet, Word576) -> Point -> (PointSet, Word576)
findNeighbours acc@(!ps, !b) p
  | b `testBit` pidx = foldl' findNeighbours (insert p ps, b `clearBit` pidx) (neighbours p)
  | otherwise = acc
 where
  pidx = fromEnum p

-- | Draws an ASCII representation of the given point sets, on a 24x24 grid.
--
-- For each point set in the argument list, points are marked with the specified character.
drawASCII :: [(Char, PointSet)] -> String
drawASCII pointsets = intercalate "\n" $ header : [ line r | r <- [0..23] ]
 where
  header = intercalate "\n"
    [ "   " ++ unwords (replicate 10 "0" ++ replicate 10 "1" ++ replicate 4 "2")
    , "   " ++ unwords (take 24 [show i | i <- cycle [(0::Int)..9]])
    ]
  prefix r = if r < 10 then '0' : show r else show r
  line r = unwords $ prefix r : do
    c <- [0..23]
    let point = Point r c
        chars = map fst $ filter (member point . snd) pointsets
    pure $ last ("." : map pure chars)

-- | Shortcut for @putStrLn . drawASCII@.
printASCII :: [(Char, PointSet)] -> IO ()
printASCII = putStrLn . drawASCII

-- | Shortcut to draw a single point set.
printASCIISingle :: PointSet -> IO ()
printASCIISingle = putStrLn . drawASCII . (:[]) . (,) 'X'
