{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE KindSignatures #-}
module Structures
  ( Structures(), Structure(..), initStructures, updateStructures, listStructures, lookupStructure
  , diffStructures
  ) where

import Control.DeepSeq
import Control.Lens
import Control.Monad
import Data.EnumSet (EnumSet)
import Data.IntMap (IntMap)
import Data.List (sortBy, foldl', sort, nub)
import Data.Maybe (mapMaybe)
import Data.List.NonEmpty (NonEmpty(..))
import Data.Ord
import Data.Semigroup
import Data.Typeable
import Data.Vector ((!), Vector, (//))
import GHC.Generics (Generic)
import SC.Types

import qualified Data.EnumMap as EnumMap
import qualified Data.EnumSet as EnumSet
import qualified Data.IntMap as IntMap
import qualified Data.Vector as Vector

import Links
import Coord
import Game
import Protocol
import Util
--------------------------------------------------------------------------------

{- $setup
>>> import Data.List
-}

-- | This data structure is used to store the peg groups that are present on the twixt board.
data Structures = Structures
  { -- | This field stores the information about the structures on the board.
    --
    -- The key of a structure in this map is its ID. Note that the IDs of structures can
    -- change when multiple structures are merged into one, so be careful if you use them to
    -- reference structures. It's generally a better idea to instead reference structures by
    -- storing a point from the structure and the lookup the index in @structuresIndex@.
    structuresData :: !(IntMap Structure)

    -- | For each point on the board, @structuresIndex@ stores the ID of the structure
    -- that the point belongs to, or zero if this point does not belong to any structure at all,
    -- which means that there is no peg at this point.
  , structuresIndex :: !(Vector Int)

    -- | @structuresConnectable@ stores the links that will connect with a structure for
    -- each point of the board.
  , structuresConnectable :: !(Vector (EnumSet Link))
  } deriving (Show, Typeable, Generic)
instance NFData Structures

instance Eq Structures where
  a == b = structuresConnectable a == structuresConnectable b
    && structures a == structures b
   where
    structures x = Vector.map (f x) (structuresIndex x)
    f x i = if i == 0 then Nothing else Just $ structuresData x IntMap.! i

instance Ord Structures where
  compare a b = compare (structuresConnectable a) (structuresConnectable b)
    <> compare (structures a) (structures b)
   where
    structures x = Vector.map (f x) (structuresIndex x)
    f x i = if i == 0 then Nothing else Just $ structuresData x IntMap.! i

-- | Information about a single peg group.
data Structure = Structure
  { -- | This field stores links starting from any point belonging to this structure
    -- which lead to a link with another structure.
    structureNeighbours :: !(Vector Link)

    -- | This field stores points which are on the minimum "edge" of the structure:
    -- this means that, viewed in the direction of the player's axis, they are very far at the
    -- beginning of the structure.
    --
    -- These are the points from which a link to the edge of the board is likely.
  , structureMinPoints :: !(Vector Point)

    -- | Like @structureMinPoints@, but for points that are at the maximum "edge" of the structure,
    -- so very far at the end.
  , structureMaxPoints :: !(Vector Point)

    -- | The player who owns the peg of this structure
  , structureOwner :: !PlayerColor
  } deriving (Show, Generic, Typeable)
instance NFData Structure

instance Eq Structure where
  a == b = c structureNeighbours && c structureMinPoints && c structureMaxPoints
    && structureOwner a == structureOwner b
   where c f = sort (Vector.toList (f a)) == sort (Vector.toList (f b))

instance Ord Structure where
  a `compare` b = c structureNeighbours <> c structureMinPoints <> c structureMaxPoints
    <> compare (structureOwner a) (structureOwner b)
   where c f = sort (Vector.toList (f a)) `compare` sort (Vector.toList (f b))

instance Semigroup Structure where
  a <> b | structureOwner a /= structureOwner b
    = error "Structure.(<>): can only merge structures owned by the same player"
  a <> b = Structure
    { structureNeighbours = mappend (structureNeighbours a) (structureNeighbours b)
    , structureMinPoints = updateMinPoints (structureMinPoints a) (structureMinPoints b)
    , structureMaxPoints = updateMaxPoints (structureMaxPoints a) (structureMaxPoints b)
    , structureOwner = player
    }
   where
    player = structureOwner a
    updateMinPoints x y = fst $ findEdgePoints player (Vector.toList $ mappend x y)
    updateMaxPoints x y = snd $ findEdgePoints player (Vector.toList $ mappend x y)

-- | Find all the possible links starting from a point in the given point group.
groupLinks :: EnumSet Point -> Links -> EnumSet Link
groupLinks group links = foldMap f (EnumSet.toList group)
 where
  f = EnumSet.fromList . filter (`linkable` links) . allPointLinks

-- | Helper function which is convenient when building a vector with @Vector.update@:
--
-- The function builds a Vector of pairs @(toEnum p, a)@ for the given points @p@ and @a@.
markAs :: a -> EnumSet Point -> Vector (Int, a)
markAs a points = Vector.fromList [(fromEnum p,a) | p <- EnumSet.toList points]

-- | Take elements from the given vector as long as it satifies the given predicated compared
-- to the first element of the vector. The predicate is passed the head of the vector as first
-- argument and the current element as second argument.
takeWhileDelta :: (a -> a -> Bool) -> Vector a -> Vector a
takeWhileDelta f vector
  | Vector.null vector = mempty
  | otherwise = Vector.takeWhile (f $ Vector.head vector) vector

-- | From points of a structure, find the edge points (returned as a pair @(minPoints, maxPoints)@)
-- (used for the @structureMaxPoints@ and @structureMinPoints@ field).
findEdgePoints :: PlayerColor -> [Point] -> (Vector Point, Vector Point)
findEdgePoints player points = (minPoints, maxPoints)
 where
  f = view $ axisCoord (playerAxis player)
  axisSorted = Vector.fromList $ sortBy (comparing f) points
  minPoints = takeWhileDelta (\z c -> f c <= f z + 1) axisSorted
  maxPoints = takeWhileDelta (\z c -> f c >= f z - 1) (Vector.reverse axisSorted)

makeUnderscoreLenses ''Structure

-- | List all available structures.
listStructures :: Structures -> [Structure]
listStructures = IntMap.elems . structuresData

-- | Lookup the structure that the given point belongs to.
lookupStructure :: Point -> Structures -> Maybe Structure
lookupStructure point Structures{structuresData, structuresIndex}
  = case structuresIndex ! fromEnum point of
    0   -> Nothing
    idx -> Just $ structuresData IntMap.! idx

-- | Create a new structures instance for the given player representing the current server state.
initStructures :: Links -> ServerState -> Structures
initStructures links server = Structures infos board conns
 where
  generateBoard e g f = Vector.accumulate g (Vector.replicate 576 e) (Vector.concatMap f structures)
  structures = Vector.indexed . Vector.unfoldr go . EnumMap.fromList $ concat
    [ map (, player) (serverPegsFor player server) | player <- [PlayerRed, PlayerBlue] ]
  go !remaining = do
    ((start, player), _) <- EnumMap.minViewWithKey remaining
    let points = EnumSet.fromList $ findConnected start links
        diffMap = EnumMap.fromAscList . map (, ()) $ EnumSet.toAscList points
    pure ((player, points), remaining `EnumMap.difference` diffMap)

  board = generateBoard 0 (flip const) $ \(idx, (_, points)) -> markAs (idx + 1) points
  conns = generateBoard mempty EnumSet.union $ \(_, (_, points)) -> Vector.fromList
    [ (idx, EnumSet.singleton $ reverseLink c)
    | c <- EnumSet.toList $ groupLinks points links
    , let idx = fromEnum $ linkTarget c
    , board ! idx == 0
    ]

  makeInfo (_, (player, points)) = Structure ns minPoints maxPoints player
   where
    possibleLinks = Vector.fromList . concatMap allPointLinks . EnumSet.toList $ points
    ns = flip Vector.filter possibleLinks $ \conn ->
      let target = fromEnum $ linkTarget conn
      in EnumSet.size (conns ! target) > 1 && EnumSet.member (reverseLink conn) (conns ! target)
    (minPoints, maxPoints) = findEdgePoints player (EnumSet.toList points)
  infos = IntMap.fromDistinctAscList . zip [1..] . Vector.toList $ Vector.map makeInfo structures

-- | Update the structures for the given move.
updateStructures :: PlayerColor -> Links -> Move -> Structures -> Structures
updateStructures player links (Move point) Structures{..} = Structures
  { structuresData = updateNeighbours . insertNew . removeConnected $ structuresData
  , structuresIndex = structuresIndex'
  , structuresConnectable = structuresConnectable'
  }
 where
  structuresIndex' = structuresIndex // indexUpdates
  structuresConnectable'
    = (Vector.accumulate (flip EnumSet.insert) ?? connectableAdditions)
    . (Vector.accumulate (flip EnumSet.delete) ?? connectableBlocks)
    $ structuresConnectable // [(fromEnum point, mempty)]

  moveLinks = EnumSet.filter targetOwnStructure $ structuresConnectable ! fromEnum point
  targetOwnStructure conn = player == structureOwner (structuresData IntMap.! idx)
   where idx = structuresIndex ! fromEnum (linkTarget conn)
  connectedIDs = EnumSet.toList $ EnumSet.fromList
    [ structuresIndex ! fromEnum (linkTarget c)
    | c <- EnumSet.toList moveLinks
    ]
  connected = map (structuresData IntMap.!) connectedIDs

  (reindex, newID)
    | (cID:rest) <- connectedIDs = (not $ null rest, cID)
    | Just ((idx,_),_) <- IntMap.maxViewWithKey structuresData = (False, idx + 1)
    | otherwise = (False, 1)
  indexUpdates = if not reindex then [(fromEnum point, newID)] else
    [(fromEnum p, newID) | p <- findConnected point links]

  connectableAdditions = Vector.fromList $
    [ (idx, reverseLink conn)
    | conn <- allPointLinks point, linkable conn links
    , let idx = fromEnum $ linkTarget conn
    , structuresIndex ! idx == 0
    ]

  connectableBlocks = Vector.fromList $
    [ (fromEnum (linkSource c), c)
    | conn <- concatMap linkBlocks (EnumSet.toList moveLinks)
    , pointValid (linkTarget conn)
    , c <- [conn, reverseLink conn]
    ]

  updatedConnectableIndices = EnumSet.toList . EnumSet.fromList . (fromEnum point :) . Vector.toList $
    mappend (Vector.map fst connectableAdditions) (Vector.map fst connectableBlocks)

  updateStructure f m conn = IntMap.adjust (f conn') idx m
   where
    conn' = reverseLink conn
    idx = structuresIndex' ! fromEnum (linkSource conn')
  updateNeighbours structures = foldl' go structures updatedConnectableIndices
   where
    f s = if EnumSet.size s <= 1 then mempty else s
    go m idx = foldl' delete (foldl' insert m toInsert) toDelete
     where
      conns' = f $ structuresConnectable' ! idx
      conns = f $ structuresConnectable ! idx
      toDelete = EnumSet.toList $ conns `EnumSet.difference` conns'
      toInsert = EnumSet.toList $ conns' `EnumSet.difference` conns
    delete = updateStructure $ \conn -> over _structureNeighbours $ Vector.filter (/= conn)
    insert = updateStructure $ \conn -> over _structureNeighbours $ Vector.cons conn

  new = Structure
    { structureMinPoints = Vector.singleton point
    , structureMaxPoints = Vector.singleton point
    , structureNeighbours = mempty
    , structureOwner = player
    }
  removeConnected = foldl' (flip IntMap.delete) ?? connectedIDs
  insertNew = IntMap.insert newID (sconcat (new :| connected))
--------------------------------------------------------------------------------

-- | Compare two structure instances, and return a human-readable string of differences between
-- the two. For debugging.
diffStructures :: Structures -> Structures -> String
diffStructures a b = unlines
  [ "* connectable info:\n" ++ unlines (map ("  " ++) diffConnectable)
  , "* structures:\n" ++ unlines (map ("  " ++) diffStructureInfo)
  ]
 where
  diffConnectable = Vector.zip (structuresConnectable a) (structuresConnectable b)
    & Vector.toList & map (\(ca,cb) -> (ca `EnumSet.difference` cb, cb `EnumSet.difference` ca))
    & zip [0..] & mapMaybe f
   where
    f (i, (pa,pb)) = do
      guard (not $ EnumSet.null (pa `mappend` pb))
      pure $ "[" ++ show (toEnum i :: Point) ++ "|" ++ show (EnumSet.toList pa) ++ "|" ++ show (EnumSet.toList pb) ++ "]"
  diffStructureInfo = go (structuresData a) (structuresData b)
  structurePoints idx = Vector.toList .
    Vector.map (toEnum . fst) . Vector.filter ((== idx) . snd) . Vector.indexed . structuresIndex
  go sa sb | Just ((aidx, s), sa') <- IntMap.minViewWithKey sa =
    let
      points = structurePoints aidx a :: [Point]
      bIDs = nub . map (structuresIndex b !) $ map fromEnum points
    in case bIDs of
      [] -> single "first" [] (aidx, s) : go sa' sb
      [bidx] -> case match (aidx,bidx) s points of
        (del, msg) -> msg ++ go sa' (if del then IntMap.delete bidx sb else sb)
      _ -> single "first" bIDs (aidx, s) : go sa' sb
  go _ sb = map (single "second" []) (IntMap.toList sb)

  single :: String -> [Int] -> (Int, Structure) -> String
  single w overlap (idx, _) = "[only|" ++ show idx ++ "|" ++ w ++ "|" ++ unwords (map show overlap) ++ "]"
  match (aidx, bidx) as apoints
    | not . EnumSet.null $ pointsDiff = (,) False $ pure $
      "[sub|" ++ ind ++ "|" ++ unwords (map show (EnumSet.toList pointsDiff)) ++ "]"
    | bs /= as = (,) True $ ["[diff|" ++ ind,"|" ++ show (sortStructure as),"|" ++ show (sortStructure bs),"]"]
    | otherwise = (True, [])
   where
    ind = show (aidx :: Int) ++ ":" ++ show bidx
    pointsDiff = EnumSet.fromList (structurePoints bidx b) `EnumSet.difference` EnumSet.fromList apoints
    bs = structuresData b IntMap.! bidx
  sortVector :: Ord a => Vector a -> Vector a
  sortVector = Vector.fromList . sort . Vector.toList
  sortStructure
    = over _structureNeighbours sortVector
    . over _structureMinPoints sortVector
    . over _structureMaxPoints sortVector
