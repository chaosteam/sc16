{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Protocol
  ( GameUpdate(..), ServerState(..), Move(..)
  , twixt, serverPegsFor, serverScoreFor, initServerState
  ) where

import Control.DeepSeq
import Control.Monad (join)
import Data.Hashable
import Data.Int
import Data.Semigroup
import Data.Serialize
import Data.Text (Text)
import Foreign.Storable
import GHC.Generics (Generic(..))
import Language.Haskell.TH.Lift
import SC.GameDefinition
import SC.Protocol
import SC.Types

import Coord
import Score

import qualified Data.Text as T

-- | The 'GameUpdate' captures all possible updates during the game.
--
-- In the "twixt" game,
-- all information required to fully simulate the game is already available
-- at the start, so this type has no fields.
data GameUpdate = GameUpdate

instance Storable GameUpdate where
  alignment _ = 1
  sizeOf _ = 1
  poke _ GameUpdate = return ()
  peek _ = return GameUpdate

-- | Represents all the information that the server sends us with each turn.
data ServerState = ServerState
  { serverTurn :: !Int8        -- ^ Number of turns already played
  , serverScoreRed :: !Score   -- ^ Current score of the red player
  , serverScoreBlue :: !Score  -- ^ Current score of the blue player
  , serverPegsRed :: ![Point]  -- ^ Pegs of the red player
  , serverPegsBlue :: ![Point] -- ^ Pegs of the blue player
  , serverSwamps :: ![Point]   -- ^ Swamp points
  , serverLinks :: ![(PlayerColor, Point, Point)]
  } deriving (Eq, Show, Ord, Generic)
instance Serialize ServerState

-- | Get the pegs for the given player.
serverPegsFor :: PlayerColor -> ServerState -> [Point]
serverPegsFor PlayerRed = serverPegsRed
serverPegsFor PlayerBlue = serverPegsBlue

-- | Get the score for the given player.
serverScoreFor :: PlayerColor -> ServerState -> Score
serverScoreFor PlayerRed = serverScoreRed
serverScoreFor PlayerBlue = serverScoreBlue

-- | An empty server state, without any pegs or swamps.
initServerState :: ServerState
initServerState = ServerState
  { serverTurn = 0
  , serverScoreRed = 0
  , serverScoreBlue = 0
  , serverPegsRed = mempty
  , serverPegsBlue = mempty
  , serverSwamps = [Point 0 0, Point 23 23, Point 23 0, Point 0 23]
  , serverLinks = mempty
  }

-- | There is only one type of Move in the twixt game: placing a peg
-- at a given point.
newtype Move = Move Point
  deriving (Eq, Ord, Show, Generic, Storable, Hashable, NFData)
deriveLift ''Move

-- | Game definition for the "Twixt" game.
twixt :: GameDefinition ServerState Move GameUpdate
twixt = GameDefinition
  { gameType = "swc_2016_sixpack"
  , mementoParser = parser
  , sendMove = send
  }

-- | Send a move to the server.
send :: Monad m => Move -> Producer m Event
send (Move Point{..}) = sendTag "data"
  [ ("class", "move")
  , ("x"    , T.pack (show _col))
  , ("y"    , T.pack (show _row))
  ]

-- | Parse a move from the given attributes.
moveAttributes :: AttributeParser Move
moveAttributes = Move <$> pointAttributes "x" "y"

-- | Parse a point from the 'x' and 'y' attributes.
pointAttributes :: Name -> Name -> AttributeParser Point
pointAttributes xAttr yAttr
  = mkPoint <$> readAttribute xAttr <*> readAttribute yAttr
 where
  mkPoint x y = Point { _col = x, _row = y }


-- | Find the first free field of the board.
parseFreeField :: Parser' Point
parseFreeField = tag "field" $ "type" =?= "NORMAL"
  *> optionalAttribute "owner" checkNotPresent
  *> pointAttributes "x" "y"
 where
  checkNotPresent (Just _) = Nothing
  checkNotPresent Nothing  = Just ()

-- | Parse a field from the board part of a memento message.
--
-- Returns type (NORMAL/SWAMP/RED/BLUE), owner and position of the field.
parseField :: Parser' (Text, Maybe PlayerColor, Point)
parseField = tag "field" $ (,,)
  <$> textAttribute "type"
  <*> optionalAttribute "owner" (Just . join . fmap readPlayerColorL)
  <*> pointAttributes "x" "y"

-- | Parse the field data of a memento message.
parseFields :: Parser' ([Point], [Point], [Point])
parseFields = go 576 mempty
 where
  go :: Int -> ([Point], [Point], [Point]) -> Parser' ([Point], [Point], [Point])
  go 0 !b = pure b
  go n !b = do
    (typ, c, p) <- parseField
    go (n - 1) (mk typ c p <> b)

  mk "SWAMP"  _ p = ([], [], pure p)
  mk _ (Just PlayerRed) p = (pure p, [], [])
  mk _ (Just PlayerBlue) p = ([], pure p, [])
  mk _        _ _ = mempty

-- | Parse all connections sent in the memento message.
--
-- Returns a list of @(OWNER, FROM, TO)@ tuples.
parseConnections :: Parser' [(PlayerColor, Point, Point)]
parseConnections = untilEOF parseConnection

-- | Parse a single connection tag.
parseConnection :: Parser' (PlayerColor, Point, Point)
parseConnection = tag "connection" $ (,,)
  <$> attribute "owner" readPlayerColorL
  <*> pointAttributes "x1" "y1"
  <*> pointAttributes "x2" "y2"

-- | Parse the memento message sent after each turn by the server.
parser :: PlayerColor -> MementoParser ServerState Move GameUpdate
parser _ownColor = MementoParser (readAttribute "turn") $ \turn _nextColor -> do
  guardedScore <- guarded $ parsePlayerData (Score <$> readAttribute "points")
  (fallbackMove, guardedState) <- parentName "board" $ do
    fallback <- fmap Move . lookahead . parentName "fields" $ parseFreeField
    fmap ((,) fallback) $ guarded $ do
      (pegsRed, pegsBlue, swamps) <- parentName "fields" parseFields
      connections <- parentName "connections" parseConnections
      pure $ \score -> ServerState
        { serverTurn = turn
        , serverScoreRed = _red score
        , serverScoreBlue = _blue score
        , serverPegsRed = pegsRed
        , serverPegsBlue = pegsBlue
        , serverSwamps = swamps
        , serverLinks = connections
        }
  previousMove <- guarded $ tag "lastMove" moveAttributes
  pure $ TurnState
    (guardedState <*> guardedScore) fallbackMove previousMove GameUpdate
