{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE BangPatterns #-}
module Game
  ( Link(..), makeLink, linkTarget, reverseLink, linkEnds, linkBlocks, allPointLinks, linkValid
  , turnNext, turnMoveCount
  , TwixtState(..)
  ) where

import Control.DeepSeq
import Control.Lens
import Data.Bits
import Data.Int
import Data.Ord
import SC.Types
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen (suchThat)
import Data.Vector.Unboxed.Deriving
import GHC.Exts (build)
import Instances.TH.Lift()
import Language.Haskell.TH.Lift

import Coord
--------------------------------------------------------------------------------

{- $setup
>>> import Data.List
>>> :{
  let
    toPointPair l = if b < a then f (b,a) else f (a,b)
     where
      a = linkSource l
      b = linkTarget l
      f (x,y) = (x^.row, x^.col, y^.row, y^.col)
:}
-}

-- | Data type representing a twixt link between two points.
data Link = Link
  { linkSource    :: !Point -- ^ The starting point of the connection
  , linkDirection :: !Arrow -- ^ Arrow from the connections' starting point to the endpoint
  }
derivingUnbox "Link" [t| Link -> Int |] [| fromEnum |] [| toEnum |]
deriveLift ''Link
instance NFData Link where rnf !_ = ()

instance Arbitrary Link where
  arbitrary = (Link <$> arbitrary <*> arbitrary) `suchThat` (pointValid . linkTarget)

instance Eq Link where
  a == b = linkEnds a == linkEnds b

instance Ord Link where
  compare = comparing linkEnds

-- | prop> toEnum (fromEnum c) == (c :: Link)
instance Enum Link where
  toEnum w = Link (toEnum $ w .&. 0x3FF) (toEnum $ w `shiftR` 10)
  fromEnum (Link p a) = fromEnum p .|. (fromEnum a `shiftL` 10)

instance Show Link where
  showsPrec d (Link a b) = showParen (d >= 11) $
    showString "Link " . showsPrec 11 a . showString " " . showsPrec 11 b

-- | Make a link from the given source point and arrow. Returns Nothing if the target point
-- of the link would be invalid or does not satisfy the specified predicate.
--
-- prop> makeLink (const False) p a == Nothing
-- prop> pointValid (applyArrow a p) ==> makeLink (const True) p a == Just (Link p a)
-- prop> not (pointValid (applyArrow a p)) ==> makeLink (const True) p a == Nothing
makeLink :: (Point -> Bool) -> Point -> Arrow -> Maybe Link
makeLink f point arrow = if pointValid target && f target then Just c else Nothing
 where
  c = Link point arrow
  target = linkTarget c
{-# INLINE makeLink #-}

-- | Return all possible links starting at the given point.
allPointLinks :: Point -> [Link]
allPointLinks point = [ Link point arrow | arrow <- allArrows, pointValid (applyArrow arrow point) ]

-- | Check whether the given link is valid (source and target points are valid).
linkValid :: Link -> Bool
linkValid c = pointValid (linkSource c) && pointValid (linkTarget c)

-- | Get the target point of the given link.
linkTarget :: Link -> Point
linkTarget (Link p a) = applyArrow a p

-- | Mirror this link, swapping source and target point.
reverseLink :: Link -> Link
reverseLink (Link p a) = Link (applyArrow a p) (reverseArrow a)

-- | Get the two points which are connected by this link.
--
-- The first point of the returned pair is always lower than the second point.
--
-- prop> fst (linkEnds c) < snd (linkEnds c)
linkEnds :: Link -> (Point, Point)
linkEnds (Link p1 a) = if p1 < p2 then (p1,p2) else (p2,p1)
 where
  p2 = applyArrow a p1

-- | Return all links that are blocked by the given link.
--
-- >>> let link = Link (Point 3 3) (makeArrow dDown False)
-- >>> sort . map toPointPair $ linkBlocks link
-- [(2,4,4,3),(3,2,4,4),(3,4,4,2),(3,4,5,3),(3,5,4,3),(4,3,5,5),(4,4,5,2),(4,4,6,3),(4,5,5,3)]
linkBlocks :: Link -> [Link]
linkBlocks (Link point arrow) = build $ \c z -> ($ z)
  $ l c p10 dM' . l c p10 dO  . l c p10 dOM
  . l c p11 dM  . l c p11 dO' . l c p11 dOM'
  . l c p20 dM' . l c p20 dO
                . l c p01 dO'
 where
  l c p a r = if pointValid p && pointValid (applyArrow a p) then Link p a `c` r else r
  minor = arrowMinor arrow
  f m m' = inDirection (arrowMajor arrow) m . inDirection minor m' $ point
  p10 = f 1 0
  p20 = f 2 0
  p01 = f 0 1
  p11 = f 1 1
  reverseIfClockwise a = if arrowClockwise a then reverseArrow a else a
  dM = reverseArrowMinor arrow
  dO = arrow & reverseIfClockwise . orthogonalArrow
  dOM = reverseArrowMinor dO
  (dM', dO', dOM') = over each reverseArrow (dM, dO, dOM)
{-# INLINE linkBlocks #-}

-- | Color of the player which has to make a move at this turn number.
turnNext :: Int8 -> PlayerColor
turnNext turn = case even turn of
  True  -> PlayerRed
  False -> PlayerBlue
{-# INLINE turnNext #-}

-- | Get the number of moves that the given player has already done at this
-- turn.
turnMoveCount :: GameConfiguration -> PlayerColor -> Int8 -> Int8
turnMoveCount conf color turn = (turn - delay) `shiftR` 2
 where
  delay = if startColor conf == color then 0 else 1
{-# INLINE turnMoveCount #-}

--------------------------------------------------------------------------------
--- | An interface for accessing information about the current state of a twixt
-- game.
data TwixtState = TwixtState
  { -- | Returns True iff there is a peg by the given player at the specified point.
    twixtHasPeg :: PlayerColor -> Point -> Bool

  , -- | Returns True iff there is a swamp at the specified point
    twixtHasSwamp :: Point -> Bool

    -- | Returns True iff the given link exists
  , twixtHasLink :: Link -> Bool

    -- | Returns True iff the given link is possible.
  , twixtLinkable :: Link -> Bool

    -- | Returns all the points that are connected to the given point.
  , twixtConnected :: Point -> [Point]

    -- | The player who has to make a move at this point in the game.
  , twixtNextPlayer :: !PlayerColor
  }
