{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Rectilinear
  ( Rectilinear(..), fromPointSet
  , Edge(..), edgeStart, edgeSize, edgeDirection, edgeEnd
  ) where

import Control.DeepSeq
import Control.Lens
import Data.Int
import Data.Serialize
import GHC.Generics (Generic)

import Coord

import qualified PointSet as P
--------------------------------------------------------------------------------

-- | An edge is a line segment that is parallel to one of the coordinate axes.
-- Edges do have an orientation, so the edge AB is not equavilent to the edge BA.
data Edge = Edge
  { -- | The point at which the edge starts. This is the first point on the
    -- line segment.
    _edgeStart :: !Point

    -- | The number of points on the edge. Note that this is one plus the
    -- number of units that you need to move in the direction of the edge
    -- to get from the starting point to the endpoint of the edge.
  , _edgeSize :: !Int8

     -- | The direction of the edge. This points from the starting point of the
     -- edge to its endpoint.
  , _edgeDirection :: !Direction
  } deriving (Eq, Generic, Ord, Serialize, NFData)
makeLenses ''Edge

instance Show Edge where
  showsPrec d (Edge start size dir) = showParen (d >= 11) $ showString "Edge " .
    showsPrec 11 start . showString " " .
    showsPrec 11 size . showString " " .
    showsPrec 11 dir

-- | Get the endpoint of the edge.
edgeEnd :: Getter Edge Point
edgeEnd = to $ \(Edge start size dir) -> inDirection dir (size - 1) start
--------------------------------------------------------------------------------

-- | A rectilinear polygon is a polygon where edges are either parallel or
-- orthogonal to each other. In this case, we also require edges to be
-- parallel to one of the coordinate axes.
data Rectilinear = Rectilinear
  { -- | The edges are given in counter-clockwise order. The points
    -- that lie on the edges belong to the area of the polygon.
    rectEdges :: [Edge]

    -- | The number of points that lie inside the polygon.
  , rectArea  :: !Int
  } deriving (Eq, Generic, Ord, Serialize, NFData)

instance Show Rectilinear where
  showsPrec d (Rectilinear es a) = showParen (d >= 11) $
    showString "Rectilinear " . showsPrec 11 es . showString " " . showsPrec 11 a

-- | Takes a point set that contains the points for one polygon,
-- and returns the edges of that polygon.
--
-- >>> fromPointSet (P.fromList [Point 0 0, Point 1 0, Point 0 1, Point 1 1])
-- [Rectilinear [Edge (Point 0 0) 2 dRight,Edge (Point 0 1) 2 dDown,Edge (Point 1 1) 2 dLeft,Edge (Point 1 0) 2 dUp] 4]
fromPointSet :: P.PointSet -> [Rectilinear]
fromPointSet points =
  [ Rectilinear (go start start dRight) (P.size group)
  | group <- P.connectedComponents points
  , Just start <- [P.minValue group]
  ]
 where
  -- This function will trace the edges of the polygon, in clockwise direction.
  go start estart dir = edge : next
   where
    -- The normal points away from the polygon. This works since we're
    -- tracing in clockwise direction, and nextDirection returns
    -- the next direction in anti-clockwise direction.
    normal = nextDirection dir

    -- Follow the current edge and return the length of it and the direction
    -- for the next edge.
    follow p !lenAcc
      -- The following condition is true if there is a point which would
      -- be at the outside of the polygon (remember, the normal points
      -- away from the polygon so any point on the same side as the normal
      -- is on the outside) if we continued this edge, so we stop.
      | inDirection normal 1 p `P.member` points = (lenAcc, normal)

      -- Otherwise, if there is still a point to continue an edge, do so.
      | p' `P.member` points = follow p' (1 + lenAcc)

      -- There's now only one way to continue the edge, since we disallow 180° turns.
      | otherwise = (lenAcc, reverseDirection normal)
     where
      p' = inDirection dir 1 p

    (len, dir') = follow estart 1
    edge = Edge estart len dir

    next
      -- We have reached the point where we started,
      -- so the polygon is closed and we stop.
      | edge^.edgeEnd == start = []
      | otherwise = go start (edge^.edgeEnd) dir'
--------------------------------------------------------------------------------
