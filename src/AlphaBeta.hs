{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
module AlphaBeta
  ( Statistic(..)
  , showStatistic
  , SearchM(), runSearchFull, evalSearchFull, runSearchBounded, alphaBeta, increaseDepth
  , withUpperBound, withLowerBound, leafStatistic, cutoffStatistic
  , Player(..), Enemy, ScoreOf(..), enemyScore
  , toRelativeScore, fromRelativeScore
  , toFirstScore, fromFirstScore
  , toSecondScore, fromSecondScore
  , fromRedScore
  ) where

import Control.Applicative
import Control.DeepSeq.Generics
import Control.Lens
import Control.Monad.RSS.Strict hiding ((<>))
import Data.Semigroup
import GHC.Base (oneShot)
import GHC.Generics (Generic)
import Prelude
import SC.Types

import qualified Data.IntMap as M

import Score

data Statistic = Statistic
  { searchedNodes  :: !(M.IntMap Int)
  , searchedLeaves :: !Int
  , minimalDepth   :: !Int
  , maximalDepth   :: !Int
  , minimalScore   :: !Score
  , maximalScore   :: !Score
  } deriving (Generic)
instance NFData Statistic where rnf = genericRnf

instance Semigroup Statistic where
  (Statistic a b c d e f) <> (Statistic a' b' c' d' e' f') =
    Statistic (M.unionWith (+) a a') (b + b') (min c c') (max d d') (min e e') (max f f')

instance Monoid Statistic where
  mempty  = Statistic M.empty 0 maxBound minBound maxBound minBound
  mappend = (<>)

showStatistic :: Statistic -> String
showStatistic statistic = concat
  [ "Leaves searched: " ++ show (searchedLeaves statistic)
  , "\t Depth: " ++ maybe "-" show (maybeMin $ minimalDepth statistic)
  , "/"          ++ maybe "-" show (maybeMax $ maximalDepth statistic)
  , "\t Score: " ++ maybe "-" show (maybeMin $ minimalScore statistic)
  , "/"          ++ maybe "-" show (maybeMax $ maximalScore statistic)
  , "\t Nodes: " ++ unwords (map showDepthStatistic $ M.toList $ searchedNodes statistic)
  ]
 where
  showDepthStatistic (depth, count) = show depth ++ ":" ++ show count
  maybeMin x = if x == maxBound then Nothing else Just x
  maybeMax x = if x == minBound then Nothing else Just x

--------------------------------------------------------------------------------
data Player = FirstPlayer | SecondPlayer

type family Enemy p where
  Enemy 'FirstPlayer = 'SecondPlayer
  Enemy 'SecondPlayer = 'FirstPlayer

-- | Score from the view of the given player. For the given player, higher points
-- represent better situations.
newtype ScoreOf (player :: Player) = ScoreOf { getScoreOf :: Score }
  deriving (Generic, Bounded, Num, Real, Read, Eq, Ord, Enum, NFData)
instance Show (ScoreOf player) where show (ScoreOf p) = show p

-- | Get the points of a first player. This is the same as getScoreOf, but
-- safer since it actually requires the type to match.
toFirstScore :: ScoreOf 'FirstPlayer -> Score
toFirstScore = getScoreOf

-- | Get the points of a second player.
toSecondScore :: ScoreOf 'SecondPlayer -> Score
toSecondScore = getScoreOf

-- | Get the points relative to the current player.
toRelativeScore :: ScoreOf p -> Score
toRelativeScore = getScoreOf

-- | Change between points from our view and points from the view of the enemy.
enemyScore :: Iso' (ScoreOf p) (ScoreOf (Enemy p))
enemyScore = iso (ScoreOf . negate . getScoreOf) (ScoreOf . negate . getScoreOf)

data LevelInfo p = LevelInfo
  { -- | The current search depth.
    _levelDepth :: !Int

    -- We implement negamax, so alpha and beta scores are always computed relative to
    -- the *current player at this level*. This means that alpha will be the best score
    -- that the *enemy* can reach when @isEnemy@ is True and also that it will be calculated
    -- from the viewpoint of the enemy, such that higher alpha values stand for a better
    -- situation *for the enemy*.
    --
    -- If @isEnemy@ is False, then alpha will represent our best score, now calculated
    -- from our point of view. So in that case, higher alpha values mean a better
    -- situation for us.

    -- | This is the minimum score that we are currently able to reach.
  , _levelAlpha :: !(ScoreOf p)

    -- | This is the maximum score that the enemy is currenlty able to reach (from
    -- our point of view).
  , _levelBeta  :: !(ScoreOf p)
  } deriving Generic
instance NFData (LevelInfo p) where rnf = genericRnf
makeLenses ''LevelInfo

newtype SearchM p s a = SearchM (RSS (LevelInfo p) Statistic s a)
  deriving (Functor, Applicative, Monad, MonadState s, MonadWriter Statistic,
            MonadReader (LevelInfo p))

getDepth :: SearchM d s Int
getDepth = view levelDepth

isEnemyTurn :: SearchM d s Bool
isEnemyTurn = odd <$> getDepth

tellStatistic :: (Int -> Statistic) -> SearchM d s ()
tellStatistic f = getDepth >>= tell . f

cutoffStatistic :: SearchM d s ()
cutoffStatistic = return ()

leafStatistic :: Score -> SearchM d s ()
leafStatistic score = tellStatistic $ \depth ->
  Statistic M.empty 1 depth depth score score

runSearchFull :: SearchM 'FirstPlayer s a -> s -> (a, s, Statistic)
runSearchFull = runSearchBounded (minBound, maxBound)

evalSearchFull :: SearchM 'FirstPlayer s a -> s -> a
evalSearchFull s = view _1 . runSearchFull s

runSearchBounded :: (Score, Score) -> SearchM 'FirstPlayer s a -> s -> (a, s, Statistic)
runSearchBounded (low, up) (SearchM action) = runRSS action (LevelInfo 0 low' up') where
 low' = ScoreOf low
 up' = ScoreOf up

forceState :: (Monad m, MonadState s m, MonadReader r m) => m a -> m a
forceState action = do
  !_ <- get
  !_ <- ask
  action

-- | Run a search at increased depth. This will also change the current player.
increaseDepth :: SearchM (Enemy d) s a -> SearchM d s a
increaseDepth (SearchM a) = SearchM $ withRSS f (forceState a) where
  f (LevelInfo d alpha beta) s = (LevelInfo (d + 1) alpha' beta', s) where
    alpha' = beta^.enemyScore
    beta' = alpha^.enemyScore

-- | Perform the given action, but only if the supplied score does not cause a cutoff.
-- If it does, the value is returned and the action is not performed.
unlessCutoff :: (ScoreOf p, a) -> SearchM p s (ScoreOf p, a)
             -> SearchM p s (ScoreOf p, a)
unlessCutoff best action = do
  beta <- view levelBeta
  if fst best >= beta
     -- The move is so good that nobody would ever play the parent move, since there
     -- is already a better candidate (remember: beta is the minimum that the opponent
     -- can currently reach, and since best >= beta, the opponent already has a better
     -- move available).
     -- Don't bother searching for even better moves, since we don't need to know
     -- exactly how bad the move was.
     then best <$ cutoffStatistic
     else action

-- | Add a new lower bound for the our current best move. This will raise alpha if
-- the given score is higher than the current alpha.
withLowerBound :: ScoreOf p -> SearchM p s a -> SearchM p s a
withLowerBound lower = local (levelAlpha %~ max lower) . forceState

-- | Add a new upper bound for the best enemy move. This will lower beta if the
-- the given score is less than the current beta.
withUpperBound :: ScoreOf p -> SearchM p s a -> SearchM p s a
withUpperBound upper = local (levelBeta %~ min upper) . forceState

-- | Search the given moves, cutting where possible.
--
-- The choices should all return the score from the view of the next player, while
-- the score returned this function will be from the view of the current player.
--
-- This function returns the first alternative if the alpha-beta window is so narrow that no move
-- falls inside the window.
alphaBeta
  :: [move]
  -> (move -> SearchM (Enemy p) s (ScoreOf (Enemy p), a))
  -> SearchM p s (ScoreOf p, a)
alphaBeta moves search = foldr f return moves (minBound, error "alphaBeta: empty list")
 where
  f move go = oneShot $ \ !best -> withLowerBound (fst best) $ do
    tellStatistic $ \depth -> mempty { searchedNodes = M.singleton depth 1 }
    val <- over _1 (review enemyScore) <$> increaseDepth (search move)
    unlessCutoff val $ go (chooseBest best val)

  chooseBest a b
    | fst b > fst a = b
    | otherwise = a
{-# INLINE alphaBeta #-}

-- | Convert from points from the view of the player who made the first move in this simulation
-- to points from the view of the current player.
fromFirstScore :: Score -> SearchM p s (ScoreOf p)
fromFirstScore p = do
  isEnemy <- isEnemyTurn
  fromRelativeScore $ if isEnemy then -p else p

-- | Convert from points from the view of the red player to points from the view of the current player.
-- The function takes the color of the player who made the first move in this simulation as an argument.
fromRedScore :: PlayerColor -> Score -> SearchM p s (ScoreOf p)
fromRedScore PlayerRed = fromFirstScore
fromRedScore PlayerBlue = fromSecondScore

-- | Like 'fromFirstScore', but for the second player.
fromSecondScore :: Score -> SearchM p s (ScoreOf p)
fromSecondScore p = do
  isEnemy <- isEnemyTurn
  fromRelativeScore $ if isEnemy then p else -p

-- | Wrap points that are already from the view of the current player.
fromRelativeScore :: Score -> SearchM p s (ScoreOf p)
fromRelativeScore = return . ScoreOf
