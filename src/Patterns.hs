{-# LANGUAGE TemplateHaskell #-}
module Patterns where

import Data.Vector (Vector)

import qualified Data.Vector as Vector

import Pattern

defensive :: Vector Pattern
defensive = Vector.fromList $(patternsFromFile "Def" "patterns.properties")

defensiveSymmetric :: Vector Pattern
defensiveSymmetric = Vector.fromList $(patternsFromFile "DefS" "patterns.properties")

offensive :: Vector Pattern
offensive = Vector.fromList $(patternsFromFile "Off" "patterns.properties")

offensiveSymmetric :: Vector Pattern
offensiveSymmetric = Vector.fromList $(patternsFromFile "OffS" "patterns.properties")
