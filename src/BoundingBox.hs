{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
-- | This module implements a bounding box type and related operations.
module BoundingBox
  ( BoundingBox(..), boundTopLeft, boundBottomRight
  , isEmpty
  , member
  , singleton
  , width
  , height
  , extentAlong
  , ofPointSet
  ) where

import Control.Lens hiding (elements)
import Data.Int
import Data.Semigroup
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Test.QuickCheck

import Coord

import qualified PointSet

-- | A bounding box is a rectangle for approximating areas.
--
-- For a non-empty bounding box, the coordinates of the bottom right corner
-- must be strictly larger than the coordinates of the top left corner.
-- In all other cases, the bounding box is considered to be empty.
data BoundingBox = BoundingBox
  { _boundTopLeft :: !Point -- ^ The top-left corner of the bounding box, inclusive
  , _boundBottomRight :: !Point -- ^ The bottom-right corner of the bounding box, exclusive
  } deriving (Eq, Ord, Generic, Serialize)
makeLenses ''BoundingBox

instance Arbitrary BoundingBox where
  arbitrary = do
    p1 <- arbitrary
    h <- elements [0..(maxBound - p1^.row)]
    w <- elements [0..(maxBound - p1^.col)]
    pure $ BoundingBox p1 (p1 & row +~ h & col +~ w)

instance Show BoundingBox where
  showsPrec d (BoundingBox p1 p2) = showParen (d >= 11) $
    showString "BoundingBox " . showsPrec 11 p1 . showString " " . showsPrec 11 p2

-- | The 'Monoid' instance combines two bounding boxes such that the resulting
-- bounding box contains both bounding boxes.
--
-- prop> not $ member p mempty
-- prop> (a <> b) <> (c :: BoundingBox) == a <> (b <> c)
-- prop> a <> mempty == (a :: BoundingBox)
-- prop> mempty <> a == (a :: BoundingBox)
-- prop> member p bb ==> member p (bb <> bb')
instance Monoid BoundingBox where
  mempty = BoundingBox (Point maxBound maxBound) (Point minBound minBound)
  mappend (BoundingBox a1 a2) (BoundingBox b1 b2) = BoundingBox
    { _boundTopLeft = zipPoints min a1 b1
    , _boundBottomRight = zipPoints max a2 b2
    }
   where
    zipPoints f (Point pr pc) (Point pr' pc') = Point (f pr pr') (f pc pc')
instance Semigroup BoundingBox where (<>) = mappend

-- | Return True iff the bounding box is empty.
isEmpty :: BoundingBox -> Bool
isEmpty (BoundingBox p q) = q^.col <= p^.col || q^.row <= p^.row

-- | Return True iff a given 'Point' is inside the specified bounding box.
--
-- >>> let bb = BoundingBox (Point 3 3) (Point 5 6)
-- >>> member (Point 3 3) bb
-- True
-- >>> member (Point 4 3) bb
-- True
-- >>> member (Point 5 3) bb
-- False
--
-- prop> let bb = BoundingBox p q in not (isEmpty bb) ==> member p bb
-- prop> let bb = BoundingBox p q in not (isEmpty bb) ==> not $ member q bb
member :: Point -> BoundingBox -> Bool
member (Point a b) box = f (>=) (box^.boundTopLeft) && f (<) (box^.boundBottomRight)
 where
  f g (Point a' b') = g a a' && g b b'

-- | Build a box with only a single point inside.
singleton :: Point -> BoundingBox
singleton point = BoundingBox point $ point & col +~ 1 & row +~ 1

-- | Return the height of the bounding box (number of rows occupied by the bounding box).
--
-- prop> height (bb1 <> bb2) >= max (height bb1) (height bb2)
height :: BoundingBox -> Int8
height (BoundingBox p1 p2) = max 0 $ p2^.row - p1^.row

-- | Return the width of the bounding box (number of columns occupied by the bounding box).
--
-- prop> width (bb1 <> bb2) >= max (width bb1) (width bb2)
width :: BoundingBox -> Int8
width (BoundingBox p1 p2) = max 0 $ p2^.col - p1^.col

-- | Get the size of the bounding box along the given axis.
--
-- For the @Col@ axis, this returns the width of the bounding box.
-- For the @Row@ axis, it returns the height of the bounding box.
extentAlong :: Axis -> BoundingBox -> Int8
extentAlong Col = width
extentAlong Row = height

-- | Calculate the bounding box of a point set.
ofPointSet :: PointSet.PointSet -> BoundingBox
ofPointSet = foldMap singleton . PointSet.toList
