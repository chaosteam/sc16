-- | This module provides more complex geometric functions,
-- building on top of the basic ones found in the Coord module
{-# LANGUAGE BangPatterns #-}
module Geometry
  ( arrowPoints
  , rasterLineSide
  , beamAreaPoints
  , approxArrows

  , cardinalSide
  ) where

import Control.Lens
import Data.Int
import SC.Types

import Coord
import PointSet (PointSet)

import qualified PointSet as PointSet
--------------------------------------------------------------------------------

{- $setup
>>> import Data.List
>>> import Data.Monoid
>>> let r = fromEnum . view row; c = fromEnum . view col; nlin a b = not $ c a * r b == c b * r a
>>> let diff as bs = [(a, b, i) | (i, (a,b)) <- zip [0..] (zip as bs), a /= b]
>>> :{
let testCardinalRow a n x p b = diff (cardinalSide a . Point n <$> [0..23]) . take 24 $
      replicate x (Just p) ++ [Nothing | b] ++ repeat (Just $ oppositePlayerColor p)
:}
-}

-- | Returns all points reached by moving in the direction of the given arrow,
-- starting from the specified point.
arrowPoints :: Arrow -> Point -> PointSet
arrowPoints arrow = PointSet.fromList . takeWhile pointValid . iterate (applyArrow arrow)

-- | Approximate the given vector by two arrows. The specified direction lies in between
-- the pair of arrows that is returned. There's no guarrantee about which arrow is the first
-- element of the returned pair and which is the second.
--
-- >>> sort (approxArrows (Point 1 1) ^.. both) == sort [makeArrow dRight True, makeArrow dDown False]
-- True
approxArrows :: Point -> (Arrow, Arrow)
approxArrows delta@(Point dRow dCol) = (best, other)
 where
  majorAxis = if abs dRow > abs dCol then Row else Col
  majorDir = Direction majorAxis (delta^.axisCoord majorAxis >= 0)
  best = makeArrow majorDir (delta^.dirCoord (nextDirection majorDir) < 0)
  sameAxis = abs (delta^.axisCoord (orthogonalAxis majorAxis) * 2) < abs (delta^.axisCoord majorAxis)
  other = if sameAxis then reverseArrowMinor best else quadrantArrow best

-- | @rasterLineSide start dir maxSteps reference@ approximates the given line between the points
-- @start@ and @end = start + maxSteps * dir@. The points are generated in such a way that they
-- all lie on the same side of the real line as the point @reference. The points @start@ and
-- @end@ are included in the result set.
--
-- >>> :{
--   PointSet.printASCII $
--     [ ('A', rasterLineSide (Point 3 3) (Point 1 2) 6 (Point 0 0))
--     , ('B', rasterLineSide (Point 4 8) (Point 1 (-2)) maxBound (Point 0 0))
--     , ('C', rasterLineSide (Point 13 6) (Point 1 3) 5 (Point 32 0))
--     , ('D', rasterLineSide (Point 20 11) (Point 2 (-3)) maxBound (Point 23 23))
--     ]
-- :}
--    0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 2 2 2 2
--    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3
-- 00 . . . . . . . . . . . . . . . . . . . . . . . .
-- 01 . . . . . . . . . . . . . . . . . . . . . . . .
-- 02 . . . . . . . . . . . . . . . . . . . . . . . .
-- 03 . . . A A . . . . . . . . . . . . . . . . . . .
-- 04 . . . . . A A B B . . . . . . . . . . . . . . .
-- 05 . . . . . B B A A . . . . . . . . . . . . . . .
-- 06 . . . B B . . . . A A . . . . . . . . . . . . .
-- 07 . B B . . . . . . . . A A . . . . . . . . . . .
-- 08 B . . . . . . . . . . . . A A . . . . . . . . .
-- 09 . . . . . . . . . . . . . . . A . . . . . . . .
-- 10 . . . . . . . . . . . . . . . . . . . . . . . .
-- 11 . . . . . . . . . . . . . . . . . . . . . . . .
-- 12 . . . . . . . . . . . . . . . . . . . . . . . .
-- 13 . . . . . . C . . . . . . . . . . . . . . . . .
-- 14 . . . . . . . C C C . . . . . . . . . . . . . .
-- 15 . . . . . . . . . . C C C . . . . . . . . . . .
-- 16 . . . . . . . . . . . . . C C C . . . . . . . .
-- 17 . . . . . . . . . . . . . . . . C C C . . . . .
-- 18 . . . . . . . . . . . . . . . . . . . C C C . .
-- 19 . . . . . . . . . . . . . . . . . . . . . . . .
-- 20 . . . . . . . . . . . D . . . . . . . . . . . .
-- 21 . . . . . . . . . . D . . . . . . . . . . . . .
-- 22 . . . . . . . . D D . . . . . . . . . . . . . .
-- 23 . . . . . . . D . . . . . . . . . . . . . . . .
rasterLineSide :: Point -> Point -> Int8 -> Point -> PointSet
rasterLineSide start dir maxSteps reference
  | dir^.row == 0 = toEdge $ iterate (col +~ signum (dir^.col)) start
  | dir^.col == 0 = toEdge $ iterate (row +~ signum (dir^.row)) start
  | otherwise = go (fromEnum $ maxSteps * maxStepsFactor) start mempty
 where
  toEdge = PointSet.fromList . takeWhile pointValid

  normal = Point (- dir^.col) (dir^.row)
  refsign = signum (dotProduct (reference - start) normal)
  dist point = dotProduct (point - start) normal * refsign

  dirAbs = abs dir
  rowMajor = dirAbs^.col < dirAbs^.row
  maxStepsFactor = max (dirAbs^.col) (dirAbs^.row)
  nextMajor = if rowMajor then row +~ signum (dir^.row) else col +~ signum (dir^.col)
  nextMinor = if rowMajor then col +~ signum (dir^.col) else row +~ signum (dir^.row)
  adjustMinor point
    | dist point < 0 || dist point' >= 0 && dist point' < dist point = point'
    | otherwise = point
   where
    point' = nextMinor point

  go !i !point !ps
    | not $ pointValid point = ps
    | i == 0 = PointSet.insert point ps
    | otherwise = go (pred i) (adjustMinor $ nextMajor point) (PointSet.insert point ps)

-- | Returns the points which are inside the specified beam area.
--
-- A beam area looks like this:
--
-- >>> :{
--   let lstart = Point 18 10
--       lend = Point 14 18
--       startRay = Point 2 1
--       endRay = Point 1 1
--       area = beamAreaPoints (lstart,lend) startRay endRay
--       area2 = beamAreaPoints (Point 3 3, Point 3 3) (Point (-2) (-1)) (Point (-1) (-2))
--   in PointSet.printASCII $ map (over _2 $ PointSet.intersection (area <> area2))
--     [ ('-', area)
--     , ('v', rasterLineSide lstart startRay maxBound lend)
--     , ('w', rasterLineSide lend endRay maxBound lstart)
--     , ('b', rasterLineSide lstart (lend - lstart) 1 (lstart + startRay))
--     , ('x', area2)
--     ]
-- :}
--    0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 2 2 2 2
--    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3
-- 00 x x . . . . . . . . . . . . . . . . . . . . . .
-- 01 x x x . . . . . . . . . . . . . . . . . . . . .
-- 02 . x x . . . . . . . . . . . . . . . . . . . . .
-- 03 . . . x . . . . . . . . . . . . . . . . . . . .
-- 04 . . . . . . . . . . . . . . . . . . . . . . . .
-- 05 . . . . . . . . . . . . . . . . . . . . . . . .
-- 06 . . . . . . . . . . . . . . . . . . . . . . . .
-- 07 . . . . . . . . . . . . . . . . . . . . . . . .
-- 08 . . . . . . . . . . . . . . . . . . . . . . . .
-- 09 . . . . . . . . . . . . . . . . . . . . . . . .
-- 10 . . . . . . . . . . . . . . . . . . . . . . . .
-- 11 . . . . . . . . . . . . . . . . . . . . . . . .
-- 12 . . . . . . . . . . . . . . . . . . . . . . . .
-- 13 . . . . . . . . . . . . . . . . . . . . . . . .
-- 14 . . . . . . . . . . . . . . . . . . b . . . . .
-- 15 . . . . . . . . . . . . . . . . b b - w . . . .
-- 16 . . . . . . . . . . . . . . b b - - - - w . . .
-- 17 . . . . . . . . . . . . b b - - - - - - - w . .
-- 18 . . . . . . . . . . b b - - - - - - - - - - w .
-- 19 . . . . . . . . . . . v - - - - - - - - - - - w
-- 20 . . . . . . . . . . . v - - - - - - - - - - - -
-- 21 . . . . . . . . . . . . v - - - - - - - - - - -
-- 22 . . . . . . . . . . . . v - - - - - - - - - - -
-- 23 . . . . . . . . . . . . . v - - - - - - - - - -
--
-- A beam area is the area enclosed by a line segment and two rays, extending until it hits the
-- edges of the board.
--
-- /Note/: The points @lstart + startRay@ and @lend + endRay@ must lie on the same side of the
-- between the points @lstart@ and  @lend@.
beamAreaPoints
  :: (Point, Point) -- ^ The two endpoints of the baseline
  -> Point          -- ^ First ray direction vector (starts at first line segment point)
  -> Point          -- ^ Second ray direction vector (start at second line segment point)
  -> PointSet       -- ^ Points inside the beam area
beamAreaPoints (lstart, lend) startRay endRay = PointSet.fromList $ filter f allPoints
 where
  f p = all (>= 0)
    [ dist lstart lnormal p * lsign
    , dist lstart norm1 p * sign1
    , dist lend norm2 p * sign2
    ]

  ldir = lend - lstart
  lnormal = Point (ldir^.col) (-ldir^.row)
  lsign = signum (dist lstart lnormal (lstart + startRay))

  norm1 = Point (startRay^.col) (-startRay^.row)
  sign1 = signum (dist lstart norm1 (lend + endRay))

  norm2 = Point (endRay^.col) (-endRay^.row)
  sign2 = signum (dist lend norm2 (lstart + startRay))

  dist start normal point = dotProduct (point - start) normal

-- | Cardinal lines always form a triangle with one whole border of the board. This function
-- returns the player who owns that border if the given point lies inside that triangle's area,
-- or the opposite player color otherwise.
--
-- Alternatively, you can say that cardinal lines split the board into two areas, whose edges are
-- the cardinal lines plus some parts of the borders of the board. The function then returns the
-- color of the player who owns more of the border of the area which contains the specified point.
--
-- Returns @Nothing@ if the point lies exactly on the cardinal line.
--
-- Cardinal lines are represented by an arrow that points towards the corner for this cardinal line
-- and has the same direction as the cardinal line does.
--
-- >>> cardinalSide aRightDown (Point 17 14)
-- Just PlayerBlue
--
-- >>> cardinalSide aDownLeft (Point 20 2)
-- Nothing
--
-- >>> cardinalSide aRightDown (Point 9 10)
-- Just PlayerBlue
--
-- >>> testCardinalRow aLeftUp 3 5 PlayerBlue True
-- []
--
-- >>> testCardinalRow aRightDown 10 24 PlayerBlue False
-- []
--
-- >>> testCardinalRow aLeftDown 10 24 PlayerBlue False
-- []
--
-- >>> testCardinalRow aRightDown 13 4 PlayerRed True
-- []
--
-- >>> testCardinalRow aUpRight 16 15 PlayerRed False
-- []
cardinalSide :: Arrow -> Point -> Maybe PlayerColor
cardinalSide arrow originalPoint = case (point^.row * dir^.col) `compare` (point^.col * dir^.row) of
  EQ -> Nothing
  LT -> Just PlayerRed
  GT -> Just PlayerBlue
 where
  originalDir = -applyArrow arrow 0

  mirror = mirrorCoordIf (originalDir^.row < 0) row . mirrorCoordIf (originalDir^.col < 0) col
  mirrorCoordIf b l = if b then l %~ (23 -) else id

  point = mirror originalPoint - Point 1 1
  dir = abs originalDir
