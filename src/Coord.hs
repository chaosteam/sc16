{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -fno-prof-auto #-}

-- | This module implements all types and functions related to the
-- coordinate system used to identify points on the twixt board.
--
-- This module implements a coordinate system based on rows and columns.
-- The origin of the coordinate system is at the top left corner of the board.
module Coord
  ( Point(..), row, col
  , allPoints, pointValid, pointValidForPlayer, neighbours, dotProduct
  , Axis(..), axisCoord, playerAxis, orthogonalAxis, onBoardEdge
  , Direction(..), dirAxis, dirPositive, inDirection, dirCoord
  , dUp, dDown, dLeft, dRight
  , nextDirection, reverseDirection, allDirections
  , Arrow(), makeArrow, arrowClockwise
  , aUpLeft, aUpRight, aDownLeft, aDownRight, aLeftUp, aLeftDown, aRightUp, aRightDown
  , nextArrow, orthogonalArrow
  , reverseArrow, reverseArrowMajor, reverseArrowMinor
  , arrowTo, applyArrow
  , arrowMajor, arrowMinor
  , quadrantArrow
  , allArrows, directionArrows, steepArrows, calmArrows
  ) where

import Control.DeepSeq
import Control.Lens hiding (elements)
import Data.Bits
import Data.Bool
import Data.Hashable
import Data.Int
import Data.Serialize
import Data.Tuple
import Data.Vector.Unboxed.Deriving
import Foreign.Ptr
import Foreign.Storable
import GHC.Generics
import Instances.TH.Lift()
import Language.Haskell.TH.Lift
import Numeric.Lens (negated)
import SC.Types
import Test.QuickCheck

import Util
--------------------------------------------------------------------------------

{- $setup
>>> import Test.QuickCheck.Monadic
>>> import Foreign.Marshal.Alloc
>>> import Data.List
-}

-- | A point represents a single position on the game board.
--
-- For convenience, points are able to contain negative coordinates, but such
-- a value is not a valid point and should not be passed to any function that
-- expects a valid point.
--
-- Allowing negative coordinates makes edge case handling easier,
-- since this way an invalid coordinate doesn't immediately overflow.
data Point = Point
  { _row :: !Int8
  , _col :: !Int8
  } deriving (Eq, Ord, Generic)
makeLenses ''Point

derivingUnbox "Point"
  [t| Point -> (Int8, Int8) |]
  [| \(Point a b) -> (a,b)  |]
  [| \(a,b) -> Point a b    |]
deriveLift ''Point

instance NFData Point where rnf !_ = ()
instance Hashable Point
instance Serialize Point
instance Arbitrary Point where arbitrary = elements allPoints

-- | This instance applies the operations pointwise on the components of the points.
-- The Enum value is used for fromInteger.
instance Num Point where
  Point a b + Point c d = Point (a + c) (b + d)
  Point a b - Point c d = Point (a - c) (b - d)
  Point a b * Point c d = Point (a * c) (b * d)
  abs (Point a b) = Point (abs a) (abs b)
  signum (Point a b) = Point (signum a) (signum b)
  fromInteger = toEnum . fromInteger

-- | A manual Show instance that hides the record accessors
instance Show Point where
  showsPrec d (Point a b) = showParen (d >= 11) $
    showString "Point " . showsPrec 11 a . showString " " . showsPrec 11 b

-- | The enum instance assigns each Point an unique index in the range
-- @[0..24*24-1] = [0..575]@
--
-- prop> fromEnum (p :: Point) `elem` [0..575]
-- prop> toEnum (fromEnum (p :: Point)) == p
instance Enum Point where
  fromEnum Point{..} = fromIntegral _row * 24 + fromIntegral _col
  toEnum b = Point (fromIntegral r) (fromIntegral c)
   where
    (r, c) = b `quotRem` 24

-- | Storable instance for points.
--
-- prop> monadicIO (assert . (== p) =<< run (alloca (\ptr -> poke ptr (p :: Point) >> peek ptr)))
-- prop> monadicIO (assert . (== p2) =<< run (alloca (\ptr -> poke ptr (p1 :: Point) >> poke ptr p2 >> peek ptr)))
instance Storable Point where
  sizeOf _ = 2
  alignment _ = 1
  peek ptr = Point <$> peek (castPtr ptr) <*> peek (ptr `plusPtr` 1)
  poke ptr (Point a b) = poke (castPtr ptr) a >> poke (ptr `plusPtr` 1) b

-- | True iff the given 'Point' represents a valid board position.
pointValid :: Point -> Bool
pointValid Point{..} = valid _row && valid _col
 where
  valid x = x >= 0 && x < 24

-- | List of all valid points.
allPoints :: [Point]
allPoints = filter pointValid $ Point <$> [0..23] <*> [0..23]

-- | True iff the point lies on the edge of the board.
onBoardEdge :: Point -> Bool
onBoardEdge (Point a b) = a == 0 || b == 0 || a == 23 || b == 23

-- | Return the neighbours of the given point.
--
-- >>> sort $ neighbours (Point 0 0)
-- [Point 0 1,Point 1 0]
-- >>> sort $ neighbours (Point 2 2)
-- [Point 1 2,Point 2 1,Point 2 3,Point 3 2]
neighbours :: Point -> [Point]
neighbours (Point a b) = filter pointValid [Point (a-1) b, Point a (b-1), Point a (b+1), Point (a+1) b]
{-# INLINE neighbours #-}

-- | Calculate the dot product of the two given vectors.
dotProduct :: Point -> Point -> Int
dotProduct (Point r1 c1) (Point r2 c2) = fromEnum r1 * fromEnum r2 + fromEnum c1 * fromEnum c2
--------------------------------------------------------------------------------

-- | An 'Axis' represents one of the two possible dimensions of the twixt board.
--
-- @Col@ is the axis that counts the columns (horizontal axis), while @Row@ is the
-- axis that counts the rows (vertical axis).
data Axis = Row | Col deriving (Eq, Enum, Bounded, Show, Ord, Generic, NFData)
derivingUnbox "Axis" [t| Axis -> Bool |] [| (== Row) |] [| \r -> if r then Row else Col |]
deriveLift ''Axis

instance Hashable Axis
instance Serialize Axis
instance Arbitrary Axis where arbitrary = arbitraryBoundedEnum

-- | Get the orthogonal axis.
orthogonalAxis :: Axis -> Axis
orthogonalAxis Row = Col
orthogonalAxis Col = Row

-- | Focus the coordinate on the given axis of a point.
axisCoord :: Axis -> Lens' Point Int8
axisCoord Row = row
axisCoord Col = col
{-# INLINE axisCoord #-}

-- | Get the axis along which the given player plays.
playerAxis :: PlayerColor -> Axis
playerAxis PlayerRed = Row
playerAxis PlayerBlue = Col

-- | True if the given point is valid for the given player.
--
-- This is not True for all points in @allPoints@, because points on the edge
-- of the board are only valid positions for the player who owns that edge.
--
-- >>> all (\p -> all (pointValidForPlayer p) (filter (not . onBoardEdge) allPoints)) [PlayerRed, PlayerBlue]
-- True
-- >>> all (pointValidForPlayer PlayerRed) allPoints
-- False
-- >>> all (pointValidForPlayer PlayerBlue) allPoints
-- False
pointValidForPlayer :: PlayerColor -> Point -> Bool
pointValidForPlayer player point = pointValid point && coord point /= 0 && coord point /= 23
 where
  coord = view . axisCoord $ orthogonalAxis (playerAxis player)

--------------------------------------------------------------------------------

-- | A 'Direction' represents on of the four directions: up, down, left or right.
data Direction = Direction
  { _dirAxis :: !Axis
  , _dirPositive :: !Bool
  } deriving (Eq, Ord, Generic, NFData)
makeLenses ''Direction
derivingUnbox "Direction" [t| Direction -> Int |] [| fromEnum |] [| toEnum |]
deriveLift ''Direction

-- | A nicer show instance
instance Show Direction where
  show d
    | d == dUp = "dUp"
    | d == dDown = "dDown"
    | d == dLeft = "dLeft"
    | otherwise = "dRight"

-- | List of all valid directions.
allDirections :: [Direction]
allDirections = [ Direction a p | a <- [Row, Col], p <- [False, True ] ]

-- | The down direction (increasing rows)
dDown :: Direction
dDown = Direction Row True

-- | The up direction (decreasing rows)
dUp :: Direction
dUp = Direction Row False

-- | The right direction (increasing columns)
dRight :: Direction
dRight = Direction Col True

-- | The left direction (decreasing columns)
dLeft :: Direction
dLeft = Direction Col False

instance Hashable Direction
instance Serialize Direction
instance Arbitrary Direction where arbitrary = Direction <$> arbitrary <*> arbitrary

-- | Assigns each Direction an unique index in the range @[0..3]@.
--
-- prop> fromEnum (e :: Direction) `elem` [0..3]
-- prop> toEnum (fromEnum (e :: Direction)) == e
instance Enum Direction where
  fromEnum (Direction a b) = setBitIf (a == Col) 1 . setBitIf b 0 $ 0
  toEnum i = Direction (bool Row Col (i `testBit` 1)) (i `testBit` 0)

-- | Sets the @/i/@th bit if the given boolean is True, returns the argument
-- unchanged otherwise.
setBitIf :: Bits a => Bool -> Int -> a -> a
setBitIf False = flip const
setBitIf True = flip setBit

-- | Rotate the direction by 90° anti-clockwise.
--
-- prop> iterate nextDirection d !! 4 == d
nextDirection :: Direction -> Direction
nextDirection (Direction a p)
  = Direction (orthogonalAxis a) (if a == Col then not p else p)

-- | Rotate the direction by 180°.
--
-- prop> reverseDirection d == nextDirection (nextDirection d)
-- prop> reverseDirection (reverseDirection d) == d
reverseDirection :: Direction -> Direction
reverseDirection = over dirPositive not

-- | @inDirection dir n p@ moves @n@ units in direction @dir@ starting from point @p@.
--
-- prop> inDirection (reverseDirection dir) n (inDirection dir n p) == p
-- prop> inDirection dir (-n) p == inDirection (reverseDirection dir) n p
inDirection :: Direction -> Int8 -> Point -> Point
inDirection (Direction r p) n = axisCoord r +~ (if p then n else -n)

-- | A lens focusing on the coordinate of a point in a given direction.
dirCoord :: Direction -> Lens' Point Int8
dirCoord dir
  | dir^.dirPositive = axisCoord (dir^.dirAxis)
  | otherwise = axisCoord (dir^.dirAxis) . negated
{-# INLINE dirCoord #-}
--------------------------------------------------------------------------------

-- | Type representing the direction of a twixt connection. Contrary to connections,
-- this type is not symmetric: an arrow from A to B is not the same as an arrow from
-- B to A.
data Arrow = Arrow
  { -- | The direction which is closest to the real arrow direction. For example,
    -- moving two units up is closest to the "up" direction.
    arrowMajor :: !Direction

    -- | True if the arrow is on the clockwise side of the direction.
    -- For example, the arrow moving two units up and one unit to the left is on the
    -- counter-clockwise side of the "up" direction, while the arrow moving two units
    -- up and one unit to the right is on the clockwise side of the "up" direction.
  , arrowClockwise :: !Bool
  } deriving (Eq, Ord, Generic)
makeUnderscoreLenses ''Arrow
derivingUnbox "Arrow" [t| Arrow -> Int |] [| fromEnum |] [| toEnum |]
deriveLift ''Arrow

-- | Construct a new arrow from a direction and clockwise argument.
makeArrow :: Direction -> Bool -> Arrow
makeArrow = Arrow

instance Hashable Arrow
instance Serialize Arrow

instance Arbitrary Arrow where arbitrary = Arrow <$> arbitrary <*> arbitrary

-- | Maps Arrows to unique indices in the range @[0..7]@.
instance Enum Arrow where
  fromEnum (Arrow dir c) = setBitIf c 2 (fromEnum dir)
  toEnum i = Arrow (toEnum i) (i `testBit` 2)

-- | A nicer show instance hiding the record selectors.
instance Show Arrow where
  show a
    | a == aUpLeft = "aUpLeft"
    | a == aUpRight = "aUpRight"
    | a == aDownLeft = "aDownLeft"
    | a == aDownRight = "aDownRight"
    | a == aLeftUp = "aLeftUp"
    | a == aLeftDown = "aLeftDown"
    | a == aRightUp = "aRightUp"
    | otherwise = "aRightDown"

-- | Construct an arrow from a tuple of row- and column-difference.
--
-- prop> length (nub [ deltaToArrow (dr, dc) | dr <- [-2..2], dc <- [-2..2], abs (dr * dc) == 2 ]) == 8
deltaToArrow :: (Int8, Int8) -> Arrow
deltaToArrow (dRow, dCol) = Arrow (Direction major p) ((bool not id p) c)
 where
  major = if abs dRow < abs dCol then Col else Row
  p = dRow == 2 || dCol == 2
  c = dRow == 1 || dCol == -1

-- | @a `arrowTo` b@ constructs an arrow representing a connection from point @a@ to
-- point @b@.
--
-- >>> Point 1 8 `arrowTo` Point 2 6
-- aLeftDown
-- >>> Point 3 3 `arrowTo` Point 1 4
-- aUpRight
-- >>> filter (\arrow -> not $ Point 3 3 `arrowTo` applyArrow arrow (Point 3 3) == arrow) allArrows
-- []
--
-- prop> a `arrowTo` applyArrow arrow a == arrow
arrowTo :: Point -> Point -> Arrow
arrowTo a b = deltaToArrow (b^.row - a^.row, b^.col - a^.col)

-- | Reverse an arrow. If you have an arrow pointing from A to B, this constructs the
-- arrow pointing from B to A.
--
-- prop> reverseArrow arrow == reverseArrowMinor (reverseArrowMajor arrow)
reverseArrow :: Arrow -> Arrow
reverseArrow = over _arrowMajor reverseDirection

-- | Reverse the minor direction of an arrow.
--
-- prop> reverseArrowMinor (reverseArrowMinor arrow) == arrow
-- prop> arrowMinor (reverseArrowMinor arrow) == reverseDirection (arrowMinor arrow)
-- prop> arrowMajor (reverseArrowMinor arrow) == arrowMajor arrow
reverseArrowMinor :: Arrow -> Arrow
reverseArrowMinor = over _arrowClockwise not

-- | Reverse the major direction of an arrow, keeping the minor direction the same.
--
-- prop> reverseArrowMajor (reverseArrowMajor arrow) == arrow
-- prop> arrowMinor (reverseArrowMajor arrow) == arrowMinor arrow
-- prop> arrowMajor (reverseArrowMajor arrow) == reverseDirection (arrowMajor arrow)
reverseArrowMajor :: Arrow -> Arrow
reverseArrowMajor = over _arrowMajor reverseDirection . over _arrowClockwise not

-- | Rotate an arrow by 90° counter-clockwise around its starting point.
--
-- prop> reverseArrow a == orthogonalArrow (orthogonalArrow a)
orthogonalArrow :: Arrow -> Arrow
orthogonalArrow = over _arrowMajor nextDirection

-- | Rotate the arrow to the next arrow in counter-clockwise direction.
--
-- prop> iterate nextArrow a !! 8 == a
-- prop> iterate nextArrow a !! 4 == reverseArrow a
-- prop> iterate nextArrow a !! 2 == orthogonalArrow a
nextArrow :: Arrow -> Arrow
nextArrow (Arrow d c) = Arrow (if not c then nextDirection d else d) (not c)

-- | Return the other arrow that lies in the same quadrant as the given arrow.
--
-- prop> quadrantArrow (quadrantArrow a) == a
-- prop> quadrantArrow a == nextArrow a || quadrantArrow a == iterate nextArrow a !! 7
-- prop> signum (applyArrow a 0) == signum (applyArrow (quadrantArrow a) 0)
quadrantArrow :: Arrow -> Arrow
quadrantArrow (Arrow d c) = Arrow d' (not c)
 where
  d' = (if c then reverseDirection else id) $ nextDirection d

-- | Return all arrows that point in the given direction. For each direction, there are four arrows
-- that point in that direction.
--
-- >>> sort $ directionArrows dUp
-- [aUpLeft,aUpRight,aLeftUp,aRightUp]
--
-- prop> sort (directionArrows dir) == sort (steepArrows dir ++ calmArrows dir)
directionArrows :: Direction -> [Arrow]
directionArrows dir
  = [Arrow dir False, Arrow dir True, Arrow (reverseDirection dir') False, Arrow dir' True]
 where
  dir' = nextDirection dir
{-# INLINE directionArrows #-}

-- | Return all arrows that have the specified direction as major direction.
--
-- prop> all ((== dir) . arrowMajor) (steepArrows dir)
steepArrows :: Direction -> [Arrow]
steepArrows dir = [Arrow dir False, Arrow dir True]
{-# INLINE steepArrows #-}

-- | Return arrows for which the minor direction is equal to the given direction.
--
-- prop> all ((== dir) . arrowMinor) (calmArrows dir)
calmArrows :: Direction -> [Arrow]
calmArrows dir = [Arrow dir' True, Arrow (reverseDirection dir') False]
 where
  dir' = nextDirection dir
{-# INLINE calmArrows #-}

-- | @applyArrow a p@ moves in the direction of the arrow starting from
-- position @p@.
--
-- prop> applyArrow (reverseArrow a) (applyArrow a p) == p
-- prop> applyArrow a (applyArrow b p) == applyArrow b (applyArrow a p)
--
-- >>> map (`applyArrow` Point 0 0) [Arrow dDown False,Arrow dDown True,Arrow dUp False,Arrow dUp True]
-- [Point 2 1,Point 2 (-1),Point (-2) (-1),Point (-2) 1]
--
-- >>> map (`applyArrow` Point 0 0) [Arrow dRight False,Arrow dRight True,Arrow dLeft False,Arrow dLeft True]
-- [Point (-1) 2,Point 1 2,Point 1 (-2),Point (-1) (-2)]
applyArrow :: Arrow -> Point -> Point
applyArrow (Arrow (Direction a p) c) point
  = point & col +~ dc & row +~ dr
 where
  (dr,dc) = case a of
    Row -> mk & _2 %~ negate
    Col -> swap mk
  sign = if p then 1 else -1
  mk = (sign * 2, if c then sign else -sign)

-- | @arrowMinor a@ returns the direction in which the arrow moves only one unit.
arrowMinor :: Arrow -> Direction
arrowMinor (Arrow d c) = mirror $ nextDirection d
 where
  mirror = if c then reverseDirection else id

-- | All valid arrows.
allArrows :: [Arrow]
allArrows = [Arrow d c | d <- allDirections, c <- [False,True]]

-- | The arrow with the major direction 'up' and minor direction 'left'.
aUpLeft :: Arrow
aUpLeft = Arrow dUp False

-- | The arrow with the major direction 'up' and minor direction 'right'.
aUpRight :: Arrow
aUpRight = Arrow dUp True

-- | The arrow with the major direction 'down' and minor direction 'left'.
aDownLeft :: Arrow
aDownLeft = Arrow dDown True

-- | The arrow with the major direction 'down' and minor direction 'right'.
aDownRight :: Arrow
aDownRight = Arrow dDown False

-- | The arrow with the major direction 'left' and minor direction 'up'.
aLeftUp :: Arrow
aLeftUp = Arrow dLeft True

-- | The arrow with the major direction 'left' and minor direction 'down'.
aLeftDown :: Arrow
aLeftDown = Arrow dLeft False

-- | The arrow with the major direction 'right' and minor direction 'up'.
aRightUp :: Arrow
aRightUp = Arrow dRight False

-- | The arrow with the major direction 'right' and minor direction 'down'.
aRightDown :: Arrow
aRightDown = Arrow dRight True
{-# INLINE allArrows #-}
