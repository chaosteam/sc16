{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
module Pattern
  ( Pattern(..), PatternCondition(..), patternsFromFile
  , checkPattern, checkPatternCondition
  , applyPatterns
  ) where

import Control.Lens
import Control.Monad
import Data.Either
import Data.Vector (Vector)
import Instances.TH.Lift()
import Language.Haskell.TH
import Language.Haskell.TH.Lift
import Language.Haskell.TH.Syntax
import SC.Types
import System.FilePath
import Text.Megaparsec
import Text.Megaparsec.Prim
import Text.Megaparsec.Text

import qualified Data.Text as Text
import qualified Data.Vector as Vector
import qualified Text.Megaparsec.Lexer as Lexer

import Coord
import Game
import Protocol

data Pattern = Pattern
  { patternMove :: !Move
  , patternConditions :: !(Vector PatternCondition)
  } deriving (Eq, Show, Ord)

data PatternCondition
  = Own Point | Enemy Point
  | Strong Point | Weak Point
  | Free Point
  | LinkPossible Link | LinkExists Link
  deriving (Eq, Show, Ord)

deriveLift ''Pattern
deriveLift ''PatternCondition

spaceConsumer :: MonadParsec s m Char => m ()
spaceConsumer = Lexer.space (void spaceChar) (Lexer.skipLineComment "#") mzero

symbol :: MonadParsec s m Char => String -> m ()
symbol = void . Lexer.symbol' spaceConsumer

lexeme :: MonadParsec s m Char => m a -> m a
lexeme = Lexer.lexeme spaceConsumer

pointParser :: MonadParsec s m Char => m Point
pointParser = Point <$> integerNum <*> integerNum
 where
  integerNum = fromInteger <$> Lexer.signed spaceConsumer (lexeme Lexer.integer)

patternConditionParser :: MonadParsec s m Char => m PatternCondition
patternConditionParser = msum
  [ fmap Own $ symbol "Own" *> pointParser
  , fmap Enemy $ symbol "Opp" *> pointParser
  , fmap Strong $ symbol "Strong" *> pointParser
  , fmap Weak $ symbol "NStrong" *> pointParser
  , fmap Free $ symbol "Free" *> pointParser
  , fmap LinkPossible $ symbol "BPoss" *> linkParser
  , fmap LinkExists $ symbol "BExist" *> linkParser
  ]
 where
  linkParser = (\a b -> Link a (a `arrowTo` b)) <$> pointParser <*> pointParser

patternParser :: MonadParsec s m Char => m (String, Pattern)
patternParser =
  (,) <$> (identifier <* identifier) <*> (many conditionOrMove >>= construct)
 where
  identifier = lexeme $ many alphaNumChar
  conditionOrMove = eitherP patternConditionParser (fmap Move $ symbol "Set" *> pointParser)
  construct args = case moves of
    [] -> fail "Pattern lacks Set command to specify move."
    [Move p] -> pure $ Pattern (Move p) (Vector.fromList (Free p : conds))
    _ -> fail "Pattern has too many Set commands: do not know which move to choose."
   where
    (conds, moves) = partitionEithers args

patternsWithKindParser :: String -> Parser [Pattern]
patternsWithKindParser kind = map snd . filter hasKind <$> (spaceConsumer *> many patternParser)
 where
  hasKind (x, _) = Text.toLower (Text.pack x) == Text.toLower (Text.pack kind)

patternsFromFile :: String -> FilePath -> ExpQ
patternsFromFile kind filename = do
  loc <- qLocation
  let fullfile = takeDirectory (loc_filename loc) </> filename
  addDependentFile fullfile
  r <- qRunIO (parseFromFile (patternsWithKindParser kind) fullfile)
  case r of
    Left err -> fail $ "Parsing patterns from <" ++ fullfile ++ "> failed: " ++ show err
    Right x -> lift x

transformPattern :: Point -> Arrow -> Pattern -> Pattern
transformPattern origin dir (Pattern (Move p) cs)
  = Pattern (Move (rel p)) (Vector.map rotateCondition cs)
 where
  rotateCondition c = case c of
    Own d -> Own (rel d)
    Enemy d -> Enemy (rel d)
    Strong d -> Strong (rel d)
    Weak d -> Weak (rel d)
    Free d -> Free (rel d)
    LinkExists l -> LinkExists (relLink l)
    LinkPossible l -> LinkPossible (relLink l)
  relLink l = Link source' (source' `arrowTo` target')
   where
    source' = rel (linkSource l)
    target' = rel (linkTarget l)
  rel d = inDirection (arrowMajor dir) (d^.row) . inDirection (arrowMinor dir) (d^.col) $ origin

checkPattern :: PlayerColor -> TwixtState -> Pattern -> Bool
checkPattern own state = Vector.all (checkPatternCondition own state) . patternConditions

checkPatternCondition :: PlayerColor -> TwixtState -> PatternCondition -> Bool
checkPatternCondition own TwixtState{..} cond = case cond of
  Own d -> pointValid d && twixtHasPeg own d
  Enemy d -> pointValid d && twixtHasPeg (oppositePlayerColor own) d
  Strong d -> pointValid d && any twixtHasLink (map (Link d) allArrows)
  Weak d -> pointValid d && not (any twixtHasLink (map (Link d) allArrows))
  Free d -> pointValid d && not (twixtHasPeg PlayerRed d || twixtHasPeg PlayerBlue d || twixtHasSwamp d)
  LinkExists l -> pointValid (linkSource l) && twixtHasLink l
  LinkPossible l -> pointValid (linkSource l) && twixtLinkable l

applyPatterns :: PlayerColor -> TwixtState -> Vector Pattern -> Vector Pattern -> (Point, Bool) -> [Move]
applyPatterns player state normal symmetric (point, forward)
  = map patternMove . filter (checkPattern player state) $ rotated
 where
  dir = Direction (playerAxis player) forward
  rotated
    =  concatMap (\p -> map (transformPattern point ?? p) (steepArrows dir)) (Vector.toList normal)
    ++ map (transformPattern point (makeArrow dir False)) (Vector.toList symmetric)
