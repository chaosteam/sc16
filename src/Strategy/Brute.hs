{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
module Strategy.Brute (strategy, initGame, updateGame, Game()) where

import Control.DeepSeq
import Control.Lens
import Data.Semigroup
import Data.List (intercalate)
import Data.Int
import SC.Types
import SC.Strategy

import AlphaBeta
import Links
import Coord
import Game
import PointSet (PointSet)
import Protocol
import Score

import qualified BoundingBox
import qualified PointSet

-- | The game state. This stores all data that needs is required for evaluation and will be
-- updated after each move.
data Game = Game
  { _playerRed :: !PlayerInfo
  , _playerBlue :: !PlayerInfo
  , _gameLinks :: !Links
  , _gameSwamps :: !PointSet
  , _gameTurn :: !Int8
  } deriving (Eq, Ord, Show)
instance NFData Game where rnf !_ = ()

-- | Information that is stored for each player.
data PlayerInfo = PlayerInfo
  { _playerPegs :: !PointSet
  , _playerScore :: !Score
  } deriving (Eq, Ord, Show)

makeLenses ''Game
makeLenses ''PlayerInfo

-- | Returns the player info for the player with the specified color.
playerFor :: PlayerColor -> Lens' Game PlayerInfo
playerFor PlayerRed = playerRed
playerFor PlayerBlue = playerBlue
{-# INLINE playerFor #-}

-- | Rates the current game situation. The returned score is higher if the game situation
-- is better for the red player.
rate :: Game -> Score
rate game = game^.playerFor PlayerRed . playerScore - game^.playerFor PlayerBlue . playerScore

-- | Initialises the game state from the server state.
initGame :: ServerState -> Game
initGame server = Game
  { _playerRed = initPlayerInfo PlayerRed server
  , _playerBlue = initPlayerInfo PlayerBlue server
  , _gameLinks = initLinks server
  , _gameSwamps = PointSet.fromList (serverSwamps server)
  , _gameTurn = serverTurn server
  }

-- | Initialises the player state for the given player with the specified server state.
initPlayerInfo :: PlayerColor -> ServerState -> PlayerInfo
initPlayerInfo player server = PlayerInfo
  { _playerScore = serverScoreFor player server
  , _playerPegs = PointSet.fromList (serverPegsFor player server)
  }

-- | Updates the game state after a move was played so that it reflects the situation after
-- the specified move.
updateGame :: Move -> Game -> Game
updateGame m game = game
  & gameLinks .~ links'
  & playerFor next %~ updatePlayerInfo next m links'
  & gameTurn +~ 1
 where
  next = turnNext (game^.gameTurn)
  pegs = game^.playerFor next.playerPegs
  links' = updateLinks (`PointSet.member` pegs) m (game^.gameLinks)

-- | Updates the player info for the given player after a move was made by this player.
-- Takes the updated links as an argument.
updatePlayerInfo :: PlayerColor -> Move -> Links -> PlayerInfo -> PlayerInfo
updatePlayerInfo player (Move point) links info = info
  & playerPegs %~ PointSet.insert point
  & playerScore %~ max (fromIntegral newScore)
 where
  newBoundingBox = foldMap BoundingBox.singleton (findConnected point links)
  newScore = BoundingBox.extentAlong (playerAxis player) newBoundingBox - 1

-- | Run an alpha-beta search, using `rate` as evaluation function, up to the specified depth.
searchDepthM :: GameConfiguration -> Int -> Game -> SearchM p () (ScoreOf p, [Move])
searchDepthM conf 0 !game = fmap (, []) $ leafStatistic 0 >> fromRedScore (selfColor conf) (rate game)
searchDepthM conf !n !game = alphaBeta (Move <$> filter availablePoint allPoints) $ \move ->
  fmap (move :) <$> searchDepthM conf (n-1) (updateGame move game)
 where
  pegs = (playerBlue <> playerRed).playerPegs
  availablePoint point = not (PointSet.member point (game^.pegs))
    && not (PointSet.member point (game^.gameSwamps))
    && pointValidForPlayer (turnNext $ game^.gameTurn) point

-- | This strategy implements the brute force strategy, which simply uses the current score
-- difference as the evaluation function for an alpha-beta search. It tries every move, no
-- filtering takes place before passing the moves on to the alpha beta search.
strategy :: StrategyCreator ServerState Move GameUpdate
strategy conf GameUpdate{} = go . initGame
 where
  searchDepth :: Int -> Game -> (String, Move, Strategy Move GameUpdate)
  searchDepth n game = (msg, move, go game')
   where
    ((_score, move:_), (), statistic) = runSearchFull (searchDepthM conf n game) ()
    game' = updateGame move game
    msg = intercalate "\n"
      [ showStatistic statistic
      , "  Turn: " ++ show (game^.gameTurn)
      , "  Move: " ++ show move
      , "  Scores: "
          ++ show (game^.playerFor PlayerRed . playerScore) ++ "/"
          ++ show (game^.playerFor PlayerBlue . playerScore)
      ]

  go :: Game -> Strategy Move GameUpdate
  go !game = Response{..}
   where
    handleEnemy :: Move -> Strategy Move GameUpdate
    handleEnemy move = go (updateGame move game)

    updateState :: GameUpdate -> Strategy Move GameUpdate
    updateState _ = go game

    moves :: [(String, Move, Strategy Move GameUpdate)]
    moves = [ searchDepth n game | n <- [1..] ]
{-# INLINE strategy #-}
