{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
module Strategy.T1J (strategy, initGame, updateGame, Game()) where

import Control.DeepSeq
import Control.Lens
import Data.Int
import Data.List (intercalate)
import Data.Semigroup
import SC.Strategy
import SC.Types

import AlphaBeta
import Links
import Coord
import Game
import Pattern
import PointSet (PointSet)
import Protocol
import Score
import T1J

import qualified BoundingBox
import qualified Patterns
import qualified PointSet

-- | The game state. This stores all data that needs is required for evaluation and will be
-- updated after each move.
data Game = Game
  { _playerRed :: !PlayerInfo
  , _playerBlue :: !PlayerInfo
  , _gameLinks :: !Links
  , gameSwamps :: !PointSet
  , _gameTurn :: !Int8
  } deriving (Eq, Ord, Show)

instance NFData Game where
  rnf !_ = ()

-- | Information that is stored for each player.
data PlayerInfo = PlayerInfo
  { _playerPegs :: !PointSet
  , _playerScore :: !Score
  , _playerT1J :: !T1J
  } deriving (Eq, Ord, Show)

makeLenses ''Game
makeLenses ''PlayerInfo

-- | Returns the player info for the player with the specified color.
playerFor :: PlayerColor -> Lens' Game PlayerInfo
playerFor PlayerRed = playerRed
playerFor PlayerBlue = playerBlue
{-# INLINE playerFor #-}

-- | Rates the current game situation. The returned score is higher if the game situation
-- is better for the red player.
rate :: Game -> Score
rate game = evaluateGameT1J PlayerRed - evaluateGameT1J PlayerBlue
 where
  evaluateGameT1J p = fst $ evaluateT1J
    p
    (gameState game)
    (game^.playerFor p.playerT1J)

-- | Initialises the game state from the server state.
initGame :: ServerState -> Game
initGame server = Game
  { _playerRed = initPlayerInfo PlayerRed state server
  , _playerBlue = initPlayerInfo PlayerBlue state server
  , _gameLinks = links
  , gameSwamps = swamps
  , _gameTurn = serverTurn server
  }
 where
  pegsRed = PointSet.fromList $ serverPegsRed server
  pegsBlue = PointSet.fromList $ serverPegsBlue server
  swamps = PointSet.fromList $ serverSwamps server
  links = initLinks server
  state = TwixtState
    { twixtHasPeg = \player point -> PointSet.member point $ case player of
        PlayerRed -> pegsRed
        PlayerBlue -> pegsBlue
    , twixtHasSwamp = PointSet.member ?? swamps
    , twixtHasLink = hasLink ?? links
    , twixtLinkable = linkable ?? links
    , twixtConnected = findConnected ?? links
    , twixtNextPlayer = turnNext (serverTurn server)
    }

-- | Initialises the player state for the given player with the specified server state.
initPlayerInfo :: PlayerColor -> TwixtState -> ServerState -> PlayerInfo
initPlayerInfo player state server = PlayerInfo
  { _playerScore = serverScoreFor player server
  , _playerPegs = PointSet.fromList (serverPegsFor player server)
  , _playerT1J = initT1J player state
  }

gameState :: Game -> TwixtState
gameState g = TwixtState
  { twixtHasPeg = \player point -> PointSet.member point . _playerPegs $ case player of
      PlayerRed -> _playerRed g
      PlayerBlue -> _playerBlue g
  , twixtHasSwamp = PointSet.member ?? gameSwamps g
  , twixtHasLink = hasLink ?? _gameLinks g
  , twixtLinkable = linkable ?? _gameLinks g
  , twixtConnected = findConnected ?? _gameLinks g
  , twixtNextPlayer = turnNext $ _gameTurn g
  }

-- | Updates the game state after a move was played so that it reflects the situation after
-- the specified move.
updateGame :: Move -> Game -> Game
updateGame m game = game
  & gameLinks .~ links'
  & playerFor next %~ updatePlayerInfo next m links'
  & updateGameT1J m
  & gameTurn +~ 1
 where
  !next = turnNext (game^.gameTurn)
  !pegs = game^.playerFor next.playerPegs
  !links' = updateLinks (`PointSet.member` pegs) m (game^.gameLinks)

updateGameT1J :: Move -> Game -> Game
updateGameT1J move game = game
  & playerRed.playerT1J %~ updateT1J PlayerRed (gameState game) move
  & playerBlue.playerT1J %~ updateT1J PlayerBlue (gameState game) move

-- | Updates the player info for the given player after a move was made by this player.
-- Takes the updated links as an argument.
updatePlayerInfo :: PlayerColor -> Move -> Links -> PlayerInfo -> PlayerInfo
updatePlayerInfo player (Move point) links info = info
  & playerPegs %~ PointSet.insert point
  & playerScore %~ max (fromIntegral newScore)
 where
  newBoundingBox = foldMap BoundingBox.singleton (findConnected point links)
  newScore = BoundingBox.extentAlong (playerAxis player) newBoundingBox - 1

generatePlayerMoves :: PlayerColor -> Game -> [Move]
generatePlayerMoves player g = crits >>= applyPatterns next state patterns patternsSym
 where
  state = gameState g
  next = turnNext (g^.gameTurn)
  offensive = player == next
  patterns = if offensive then Patterns.offensive else Patterns.defensive
  patternsSym = if offensive then Patterns.offensiveSymmetric else Patterns.defensiveSymmetric
  t1j = g^.playerFor player.playerT1J
  crits = criticalPoints player state t1j

generateMoves :: Game -> [Move]
generateMoves g = filter moveValid . addDefaultMove $
  generatePlayerMoves PlayerRed g ++ generatePlayerMoves PlayerBlue g
 where
  taken = (_playerPegs._playerRed) g <> (_playerPegs._playerBlue) g <> gameSwamps g
  moveValid (Move p) = pointValidForPlayer (turnNext (g^.gameTurn)) p && not (PointSet.member p taken)
  defaultMoves = Move <$> allPoints
  addDefaultMove [] = defaultMoves
  addDefaultMove xs = xs

-- | Run an alpha-beta search, using `rate` as evaluation function, up to the specified depth.
searchDepthM :: GameConfiguration -> Int -> Game -> SearchM p s (ScoreOf p, [Move])
searchDepthM conf 0 !game = fmap (, []) $ leafStatistic 0 >> fromRedScore (selfColor conf) (rate game)
searchDepthM conf !n !game = alphaBeta (generateMoves game) $ \move ->
  fmap (move :) <$> searchDepthM conf (n-1) (updateGame move game)

-- | This strategy implements the brute force strategy, which simply uses the current score
-- difference as the evaluation function for an alpha-beta search. It tries every move, no
-- filtering takes place before passing the moves on to the alpha beta search.
strategy :: StrategyCreator ServerState Move GameUpdate
strategy conf GameUpdate{} = go . initGame
 where
  searchDepth :: Int -> Game -> (String, Move, Strategy Move GameUpdate)
  searchDepth n game = (msg, move, go game')
   where
    ((_score, move:_), (), statistic) = runSearchFull (searchDepthM conf n game) ()
    game' = updateGame move game
    msg = intercalate "\n"
      [ showStatistic statistic
      , "  Turn: " ++ show (game^.gameTurn)
      , "  Move: " ++ show move
      , "  Scores: "
          ++ show (game^.playerFor PlayerRed . playerScore) ++ "/"
          ++ show (game^.playerFor PlayerBlue . playerScore)
      ]

  go :: Game -> Strategy Move GameUpdate
  go !game = Response{..}
   where
    handleEnemy :: Move -> Strategy Move GameUpdate
    handleEnemy move = go (updateGame move game)

    updateState :: GameUpdate -> Strategy Move GameUpdate
    updateState _ = go game

    moves :: [(String, Move, Strategy Move GameUpdate)]
    moves = [ searchDepth n game | n <- [1..] ]
{-# INLINE strategy #-}
