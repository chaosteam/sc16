{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
module Strategy.Monkey (strategy) where

import Control.Lens
import Data.HashMap.Strict (HashMap)
import Data.Hashable
import Data.Int
import Data.List (intercalate, sortBy, unfoldr)
import Data.Maybe
import Data.Monoid
import Data.Ord
import SC.Strategy
import SC.Types

import AlphaBeta
import Links
import Coord
import Game
import PointSet (PointSet)
import Protocol
import Score
import Structures

import qualified Data.HashMap.Strict as HashMap

import qualified BoundingBox
import qualified PointSet

-- | The game state. This stores all data that needs is required for evaluation and will be
-- updated after each move.
data Game = Game
  { _playerRed :: !PlayerInfo
  , _playerBlue :: !PlayerInfo
  , _gameLinks :: !Links
  , _gameStructures :: !Structures
  , _gameSwamps :: !PointSet
  , _gameTurn :: !Int8
  } deriving Show

instance Eq Game where
  a == b = _playerRed a == _playerRed b && _playerBlue a == _playerBlue b
    && _gameLinks a == _gameLinks b

instance Ord Game where
  compare a b = compare (_playerRed a) (_playerRed b) <> compare (_playerBlue a) (_playerBlue b)
    <> compare (_gameLinks a) (_gameLinks b)

instance Hashable Game where
  hashWithSalt salt a = salt `hashWithSalt` _playerRed a `hashWithSalt` _playerBlue a
    `hashWithSalt` _gameLinks a

-- | Information that is stored for each player.
data PlayerInfo = PlayerInfo
  { _playerPegs :: !PointSet
  , _playerScore :: !Score
  } deriving (Eq, Ord, Show)

instance Hashable PlayerInfo where
  hashWithSalt salt p = hashWithSalt salt (_playerPegs p)

makeLenses ''Game
makeLenses ''PlayerInfo

-- | Returns the player info for the player with the specified color.
playerFor :: PlayerColor -> Lens' Game PlayerInfo
playerFor PlayerRed = playerRed
playerFor PlayerBlue = playerBlue
{-# INLINE playerFor #-}

-- | Rates the current game situation. The returned score is higher if the game situation
-- is better for the red player.
rate :: Game -> Score
rate game = {-# SCC rate #-} game^.playerRed.playerScore - game^.playerBlue.playerScore

-- | Initialises the game state from the server state.
initGame :: ServerState -> Game
initGame server = Game
  { _playerRed = initPlayerInfo PlayerRed server
  , _playerBlue = initPlayerInfo PlayerBlue server
  , _gameLinks = links
  , _gameStructures = initStructures links server
  , _gameSwamps = PointSet.fromList (serverSwamps server)
  , _gameTurn = serverTurn server
  }
 where
  links = initLinks server

-- | Initialises the player state for the given player with the specified server state.
initPlayerInfo :: PlayerColor -> ServerState -> PlayerInfo
initPlayerInfo player server = PlayerInfo
  { _playerPegs = PointSet.fromList (serverPegsFor player server)
  , _playerScore = serverScoreFor player server
  }

-- | Updates the game state after a move was played so that it reflects the situation after
-- the specified move.
updateGame :: Move -> Game -> Game
updateGame m game = game
  & gameLinks .~ links'
  & gameStructures %~ updateStructures next links' m
  & playerFor next %~ updatePlayerInfo next links' m
  & gameTurn +~ 1
 where
  next = turnNext (game^.gameTurn)
  pegs = game^.playerFor next.playerPegs
  links' = {-# SCC updateLinks #-}
    updateLinks (`PointSet.member` pegs) m (game^.gameLinks)

-- | Updates the player info for the given player after a move was made by this player.
-- Takes the updated links as an argument.
updatePlayerInfo :: PlayerColor -> Links -> Move -> PlayerInfo -> PlayerInfo
updatePlayerInfo player links (Move point) info = {-# SCC updatePlayerInfo #-} info
  & playerPegs %~ ({-# SCC updatePegs #-} PointSet.insert point)
  & playerScore %~ ({-# SCC updateScore #-} max (fromIntegral newScore))
 where
  newBoundingBox = foldMap BoundingBox.singleton (findConnected point links)
  newScore = BoundingBox.extentAlong (playerAxis player) newBoundingBox - 1

newtype SearchState = SearchState
  { _transpositionTable :: HashMap Game Searched
  }

data Searched = Searched
  { searchedScoreRaw :: !Score
  , searchedPV :: ![Move]
  , searchedDepth :: !Int
  } deriving Show

makeLenses ''SearchState

makeSearched :: Int -> (ScoreOf p, [Move]) -> Searched
makeSearched minDepth (score, pv) = Searched (toRelativeScore score) pv minDepth

searchedResult :: Searched -> (ScoreOf p, [Move])
searchedResult s = (ScoreOf $ searchedScoreRaw s, searchedPV s)

cached
  :: Int -> Game -> SearchM p SearchState (ScoreOf p, [Move])
  -> SearchM p SearchState (ScoreOf p, [Move])
cached requiredDepth game action = do
  table <- use transpositionTable
  case HashMap.lookup game table of
    Just s | searchedDepth s >= requiredDepth -> return (searchedResult s)
    _ -> do
      result <- action
      transpositionTable %= HashMap.insert game (makeSearched requiredDepth result)
      return result

-- | Run an alpha-beta search, using `rate` as evaluation function, up to the specified depth.
searchDepthM :: GameConfiguration -> Int -> Game -> SearchM p SearchState (ScoreOf p, [Move])
searchDepthM conf 0 !game = fmap (, []) $ leafStatistic 0 >> fromRedScore (selfColor conf) (rate game)
searchDepthM conf !n !game = cached n game $ do
  table <- use transpositionTable
  let pvMove = do
        s <- HashMap.lookup game table
        m <- listToMaybe (searchedPV s)
        pure (fmap (m :) <$> searchDepthM conf (n-1) (updateGame m game))
  flip alphaBeta id . maybe id (:) pvMove . order $ map Move points <&> \move ->
    let
      game' = {-# SCC updateGame #-} updateGame move game
      s = HashMap.lookup game table
      rating = maybe maxBound searchedScoreRaw s
    in (rating, fmap (move :) <$> searchDepthM conf (n-1) game')
 where
  points = filter availablePoint allPoints
  next = turnNext $ game^.gameTurn
  pegs = (playerBlue.playerPegs <> playerRed.playerPegs)
  availablePoint point = not (PointSet.member point (game^.pegs))
    && not (PointSet.member point (game^.gameSwamps))
    && pointValidForPlayer next point
  order = map snd . sortBy (flip $ comparing fst)

strategy :: StrategyCreator ServerState Move GameUpdate
strategy conf GameUpdate{} = go (SearchState mempty) . initGame
 where
  searchDepth
    :: Game -> (Int, SearchState)
    -> ((String, Move, Strategy Move GameUpdate), (Int, SearchState))
  searchDepth game (n,s) =
    let
      ((_score, move:_), s', statistic) = ({-# SCC search #-} runSearchFull (searchDepthM conf n game)) s
      game' = updateGame move game
      msg = intercalate "\n"
        [ showStatistic statistic
        , "  Turn: " ++ show (game^.gameTurn)
        , "  Move: " ++ show move
        , "  Scores: "
            ++ show (game^.playerRed.playerScore) ++ "/"
            ++ show (game^.playerBlue.playerScore)
        ]
    in ((msg, move, go s' game'), (n + 1, s'))

  go :: SearchState -> Game -> Strategy Move GameUpdate
  go !s !game = Response{..}
   where
    handleEnemy :: Move -> Strategy Move GameUpdate
    handleEnemy move = go s (updateGame move game)

    updateState :: GameUpdate -> Strategy Move GameUpdate
    updateState _ = go s game

    moves :: [(String, Move, Strategy Move GameUpdate)]
    moves = unfoldr (Just . searchDepth game) (1,s)
{-# INLINE strategy #-}
