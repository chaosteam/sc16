{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Score where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Hashable
import Data.Semigroup
import Data.Serialize

newtype Score = Score Int
  deriving (Real, Integral, Num, Read, Eq, Ord, Enum, NFData, Serialize, Hashable)
makeWrapped ''Score

instance Show Score where show (Score p) = show p

-- | This bounded instance guarrantes that each Score has a negation.
-- (The default instance for Int fails this because @-(minBound :: Int) == minBound@).
instance Bounded Score where
  minBound = Score $ minBound + 1
  maxBound = Score maxBound

-- | Additive 'Semigroup' for 'Score'.
instance Semigroup Score where
  Score a <> Score b = Score $ a + b

-- | Additive 'Monoid' for 'Score'
instance Monoid Score where
  mempty = Score 0
  mappend = (<>)
