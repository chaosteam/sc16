module Main where

import SC.Main

import Protocol

import qualified Strategy.Brute as Brute
import qualified Strategy.Monkey as Monkey
import qualified Strategy.T1J as T1J

main :: IO ()
main = defaultMain twixt
  [ ("monkey", Monkey.strategy)
  , ("brute", Brute.strategy)
  , ("t1j", T1J.strategy)
  , ("null", \_ _ _ -> nullStrategy)
  ]
