module Main where

import Criterion.Main

import Protocol

import StrategyBench
import RandomMoveBench

import qualified LinksBench
import qualified PointSetBench

import qualified Strategy.Brute as Brute
import qualified Strategy.Monkey as Monkey
import qualified Strategy.T1J as T1J

main :: IO ()
main = do
  loadedSituations <- loadSituations defaultSituations
  defaultMain
    [ LinksBench.benchmark
    , PointSetBench.benchmark
    , bgroup "strategies"
      [ benchStrategy 3 "brute" Brute.strategy loadedSituations
      , benchStrategy 4 "monkey" Monkey.strategy loadedSituations
      , benchStrategy 3 "t1j" T1J.strategy loadedSituations
      ]
    , bgroup "randomMove"
      [ benchRandomMove "t1j/updateGame" (T1J.initGame initServerState) T1J.updateGame
      , benchRandomMove "brute/updateGame" (Brute.initGame initServerState) Brute.updateGame
      , benchRandomMove "null" (Move 0) (\m _ -> m)
      ]
    ]
