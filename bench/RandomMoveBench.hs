{-# LANGUAGE BangPatterns #-}
module RandomMoveBench (benchRandomMove) where

import Control.Lens
import System.Random.MWC
import Control.Monad.Loops
import Control.DeepSeq
import Criterion

import Coord
import Protocol

import qualified PointSet

newtype GenNF = GenNF GenIO
instance NFData GenNF where rnf !_ = ()

randomPoint :: GenIO -> IO Point
randomPoint gen = iterateUntil (\p -> pointValid p && f p) $
  Point <$> uniformR (0,24) gen <*> uniformR (0,24) gen
 where
  f p = p^.row > 0 && p^.row < 23 && p^.col > 0 && p^.col < 23

benchRandomMove :: NFData a => String -> a -> (Move -> a -> a) -> Benchmark
benchRandomMove name start update = env (GenNF <$> createSystemRandom) $ \(GenNF gen) ->
  bench (name ++ "60") $ nfIO (go gen mempty (60 :: Int) start)
 where
  go _ _ 0 !s = return s
  go !g !ps !n !s = do
    p <- iterateUntil (not . (`PointSet.member` ps)) (randomPoint g)
    go g (PointSet.insert p ps) (n - 1) (update (Move p) s)
{-# INLINE benchRandomMove #-}
