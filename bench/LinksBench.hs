{-# LANGUAGE BangPatterns #-}
module LinksBench (benchmark) where

import Control.DeepSeq
import Control.Monad.Loops
import Criterion hiding (benchmark)
import System.Random.MWC

import Links
import Coord
import Game

newtype GenNF = GenNF GenIO
instance NFData GenNF where rnf !_ = ()

randomPoint :: GenIO -> IO Point
randomPoint gen = iterateUntil pointValid $
  Point <$> uniformR (0,24) gen <*> uniformR (0,24) gen

randomArrow :: GenIO -> IO Arrow
randomArrow gen = toEnum <$> uniformR (0,7) gen

addRandomLinks :: GenIO -> Int -> Links -> IO Links
addRandomLinks _ 0 !conns = return conns
addRandomLinks gen n !conns = do
  point <- randomPoint gen
  arrow <- randomArrow gen
  let conn = Link point arrow
  if pointValid (linkTarget conn)
    then addRandomLinks gen (n-1) (addLinkIfPossible conn conns)
    else addRandomLinks gen n conns

benchmark :: Benchmark
benchmark = env (GenNF <$> createSystemRandom) $ \(GenNF gen) -> bgroup "Links"
  [ bench "addLinkIfPossible60" $ whnfIO (addRandomLinks gen 60 emptyLinks)
  ]
