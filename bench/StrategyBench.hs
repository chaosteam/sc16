module StrategyBench
  ( benchStrategy
  , defaultSituations
  , loadSituations
  ) where

import Criterion
import Control.Lens
import Data.Proxy
import SC.Strategy
import SC.GameDefinition
import SC.Replay
import SC.Types
import System.FilePath.Posix

import Game
import Protocol

type Situations = [(String, TurnState Identity Proxy ServerState Move GameUpdate)]

benchStrategy :: Int -> String -> StrategyCreator ServerState Move GameUpdate -> Situations -> Benchmark
benchStrategy maxDepth name strategyF = bgroup name . map benchSituation where
  benchSituation (situationName, state) = bgroup situationName
    [ bench (show depth) $ nf (\x -> view _2 $ moves (strategyF conf GameUpdate x) !! depth) gstate
    | depth <- [0..maxDepth]
    ]
   where
    gstate = runIdentity . initState $ state
    conf = GameConfiguration { selfColor = turnNext $ serverTurn gstate, startColor = PlayerRed }

turnsWithInitial :: Replay init move state -> [TurnState Identity Proxy init move state]
turnsWithInitial replay = initialSituation replay : map proxyLastMove (turns replay) where
  proxyLastMove m = m { lastMove = Proxy }

loadSituations :: [(FilePath, [Int])] -> IO Situations
loadSituations files = do
  replays <- (traverse . _1) (\x -> (,) x <$> loadReplay twixt x) files
  return $ do
    ((replayPath, replay), turnIds) <- replays
    let replayName = takeFileName $ dropExtensions replayPath
    turnId <- turnIds
    turn <- turnsWithInitial replay ^.. ix turnId
    pure (replayName ++ "/" ++ show turnId, turn)

defaultSituations :: [(FilePath, [Int])]
defaultSituations = flip zip (repeat [0,10,40,55])
  [ "data/replays/bench1.xml.gz"
  ]
