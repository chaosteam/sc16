{-# LANGUAGE BangPatterns #-}
module PointSetBench (benchmark) where

import Control.DeepSeq
import Control.Monad
import Control.Applicative
import Criterion hiding (benchmark)
import System.Random.MWC
import Data.IntSet (IntSet)
import Control.Monad.Loops

import qualified Data.IntSet as IntSet

import Coord
import PointSet (PointSet)

import qualified PointSet

newtype GenNF = GenNF GenIO
instance NFData GenNF where rnf !_ = ()

randomPoint :: GenIO -> IO Point
randomPoint gen = iterateUntil pointValid $
  Point <$> uniformR (0,24) gen <*> uniformR (0,24) gen

insertRandomPS :: GenIO -> Int -> PointSet -> IO PointSet
insertRandomPS _ 0 !ps = return ps
insertRandomPS gen n !ps = randomPoint gen >>= insertRandomPS gen (n-1) . flip PointSet.insert ps

insertRandomIS :: GenIO -> Int -> IntSet -> IO IntSet
insertRandomIS _ 0 !is = return is
insertRandomIS gen n !is = randomPoint gen >>= insertRandomIS gen (n-1) . flip IntSet.insert is . fromEnum

benchmark :: Benchmark
benchmark = env (GenNF <$> createSystemRandom) $ \(GenNF gen) -> bgroup "PointSet"
  [ bgroup "insert30"
      [ bench "pointset" (nfIO (insertRandomPS gen 30 mempty))
      , bench "intset" (nfIO (insertRandomIS gen 30 IntSet.empty))
      ]
  , bgroup "insert60"
      [ bench "pointset" (nfIO (insertRandomPS gen 60 mempty))
      , bench "intset" (nfIO (insertRandomIS gen 60 IntSet.empty))
      ]
  , env (join (liftA2 (,)) (replicateM 30 (randomPoint gen))) $ \ ~(pa, pb) -> bgroup "union"
     [ bench "poinset" (nf (mappend (PointSet.fromList pa)) (PointSet.fromList pb))
     , bench "intset" (nf (IntSet.union (IntSet.fromList (map fromEnum pa))) (IntSet.fromList (map fromEnum pb)))
     ]
  , env (join (liftA2 (,)) (replicateM 30 (randomPoint gen))) $ \ ~(pa, pb) -> bgroup "intersection"
     [ bench "poinset" (nf (PointSet.intersection (PointSet.fromList pa)) (PointSet.fromList pb))
     , bench "intset" (nf (IntSet.intersection (IntSet.fromList (map fromEnum pa))) (IntSet.fromList (map fromEnum pb)))
     ]
  , env (replicateM 30 (randomPoint gen)) $ \pb -> bgroup "toList"
     [ bench "poinset" (nf PointSet.toList (PointSet.fromList pb))
     , bench "intset" (nf IntSet.toList (IntSet.fromList (map fromEnum pb)))
     ]
  ]
