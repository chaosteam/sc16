{-
This file provides a simple GUI to view twixt situations. To use it, simply load this file with
GHCi:

> ghci Main.hs

After that, you can use `setDia` and `addDia` to show information on a twixt board.
For example, to show a red peg at position (4,3), run the following from the GHCi prompt:

> setDia (atPoint (Point 4 3) (pegDia # fc red))

A window for showing the board will be created automatically when the first such command
is executed.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-type-defaults -fno-warn-missing-signatures -fno-warn-unused-imports #-}
module Main where

import Control.Concurrent
import Control.Monad
import Control.Monad.Identity
import Data.IORef
import Data.Int
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Ord
import Data.Proxy
import Data.Vector ((!))
import Diagrams.Backend.Cairo
import Diagrams.Backend.Gtk
import Diagrams.Prelude hiding (Point)
import Diagrams.TwoD.Layout.Grid
import Foreign.Store
import Graphics.UI.Gtk (AttrOp(..))
import SC.GameDefinition
import SC.Replay
import SC.Types hiding (red, blue)
import System.IO.Unsafe

import qualified Graphics.UI.Gtk as Gtk
import qualified Diagrams.Prelude as Diagrams
import qualified Data.Vector as Vector
import qualified Data.Vector.Unboxed as UVector

import Coord
import Game
import Pattern
import PointSet (PointSet)
import Protocol
import Geometry
import Links
import Structures
import T1J

import qualified PointSet
import qualified Patterns

import Debug.Trace

besideFramed :: (InSpace (V a) (N a) a, Juxtaposable a, Semigroup a) => Vn a -> a -> a -> a
besideFramed v a b = beside (negated v) (beside v b a) a

twixtGrid :: Diagram B
twixtGrid = bordered lineLabels columnLabels . bordered vborder hborder $ board
 where
  cell = strut (unitX + unitY)
  bordered v h = besideFramed unitY h . besideFramed unitX v
  vborder = vrule (height board) # lc blue # alignTL # translateY 0.5
  hborder = hrule (width board) # lc red # alignTL # translateX (-0.5)

  board = gridCat' 24 [ cell <> pointDia r c | r <- [0..23], c <- [0..23] ]
  pointDia r c = circle 0.12 # pointStyle r c
  pointStyle r c
    | (r == 0 || r == 23) && c /= 0 && c /= 23 = lc red . lineWidth thin
    | (c == 0 || c == 23) && r /= 0 && r /= 23 = lc blue . lineWidth thin
    | otherwise = lc black . lineWidth veryThin

  lineLabels = vcat [ cell <> label i | i <- [0..23] ]
  columnLabels = hcat [ cell <> label i | i <- [0..23] ]
  label i = text (show i) # fontSize (local 0.35)

pegDia :: Diagram B
pegDia = circle 0.23 # lineWidth thin

crossDia :: Diagram B
crossDia = star (StarSkip 2) (square (0.46 / sqrt 2)) # strokeP # lineWidth thin

arrowDia :: Arrow -> Diagram B
arrowDia a = arrowV' arrowOpts (pv (applyArrow a 0))
 where
  arrowOpts = with
    & arrowHead .~ arrowheadTriangle (3/7 @@ turn)
    & shaftStyle %~ lineWidth thin
    & headLength *~ 0.75

linkArrowDia :: Arrow -> Diagram B
linkArrowDia a = pp 0 ~~ pp (applyArrow a 0)

pv :: Point -> V2 Double
pv (Point r c) = unit_Y * realToFrac r + unitX * realToFrac c

pp :: Point -> Diagrams.Point V2 Double
pp = review Diagrams._Point . pv

mkPP :: Int8 -> Int8 -> Diagrams.Point V2 Double
mkPP = curry $ pp . uncurry Point

atPoint :: Point -> Diagram B -> Diagram B
atPoint = translate . pv

linkDia :: Link -> Diagram B
linkDia l = atPoint (linkSource l) $ linkArrowDia (linkDirection l)

playerColour :: PlayerColor -> Colour Double
playerColour PlayerRed = red
playerColour PlayerBlue = blue

guideLinesDia :: Diagram B
guideLinesDia = mconcat
  [ guide d | d <- allArrows ] # lineWidth ultraThin # dashingL [0.3,0.1] 0 # lc darkgray
 where
  guide d = pp (startPoint d) ~~ pp (iterate (applyArrow d) (startPoint d) !! 10)
  startPoint a = Point (f (p^.row)) (f (p^.col))
   where
    p = applyArrow a 0
    f x = if x < 0 then 22 else 1

class ToDia a where toDia :: a -> Diagram B

instance ToDia Point where toDia p = atPoint p pegDia
instance ToDia Arrow where toDia = arrowDia
instance ToDia Link where toDia = linkDia
instance ToDia a => ToDia [a] where toDia = foldMap toDia
instance ToDia PointSet where toDia = toDia . PointSet.toList
instance ToDia ServerState where
  toDia ServerState{..} = mconcat
    [ toDia serverPegsRed # fc red
    , toDia serverPegsBlue # fc blue
    , flip foldMap serverLinks $ \(p, a, b) -> toDia (Link a (a `arrowTo` b)) # lc (playerColour p)
    , toDia serverSwamps # fc lightgreen
    ]
instance ToDia Move where toDia (Move p) = atPoint p pegDia
instance ToDia Pattern where
  toDia (Pattern (Move x) cs) = mconcat
    [ pegDia # fc yellow
    , atPoint x crossDia # lineWidth thick . lc red
    , toDia $ sortBy (comparing $ not . isAttribute) (Vector.toList cs)
    ]
   where
    isAttribute (Weak _) = True
    isAttribute (Strong _) = True
    isAttribute _ = False
instance ToDia PatternCondition where
  toDia (Free p) = atPoint p crossDia
  toDia (Enemy p) = atPoint p pegDia # fc blue
  toDia (Own p) = atPoint p pegDia # fc red
  toDia (Strong p) = atPoint p (crossDia # lc green)
  toDia (Weak p) = atPoint p (crossDia # lc blue)
  toDia (LinkPossible l) = linkDia l # dashingL [0.3,0.15] 0
  toDia (LinkExists l) = linkDia l

(getDias, setDias) = actions
getDia = mconcat <$> getDias
setDia = setDias . pure
clearDia = setDia mempty
consDia = modifyDias . (:)
snocDia = modifyDias . flip mappend . pure
modifyDias f = getDias >>= setDias . f
popDiaTop = modifyDias tail
popDiaBot = modifyDias init
setTopDia d = modifyDias $ (d:) . drop 1

withReplay f = f <$> readStoreDef replayStore (fail "no replay loaded")
withTurn t f = withReplay $ \r ->
  f (initFullState . runIdentity . initState $ turnsWithInitial r !! fromIntegral t)
withCurrentTurn f = readStore turnStore >>= \t -> withTurn t f

turnTopDia f = setTopDia =<< withCurrentTurn f
turnConsDia f = consDia =<< withCurrentTurn f

withMove t f = withReplay (\r -> f (runIdentity . lastMove $ turns r !! fromIntegral t))

showTurn t = do
  clearDia
  d <- nextMovesDia t 10
  withTurn t (mappend d . toDia . server) >>= consDia

nextMovesDia t n = fmap mconcat . forM [t..(t+n-1)] $ \x -> do
  let c = playerColour (turnNext (fromIntegral x))
  withMove x $ \(Move p) -> atPoint p $ mconcat
    [ text (show (div (x - t) 2 + 1)) # fontSize (local 0.25) # bold # fc c
    , pegDia # fc white # lc c
    ]

showNextMoves t = consDia <=< nextMovesDia t

turnsWithInitial :: Replay init move state -> [TurnState Identity Proxy init move state]
turnsWithInitial replay = initialSituation replay : map proxyLastMove (turns replay) where
  proxyLastMove m = m { lastMove = Proxy }

initFullState :: ServerState -> FullState
initFullState server = S{..} where
  state = TwixtState
    { twixtHasPeg = \player peg -> elem peg $ serverPegsFor player server
    , twixtHasSwamp = (`elem` serverSwamps server)
    , twixtHasLink = (`hasLink` links)
    , twixtLinkable = (`linkable` links)
    , twixtConnected = (`findConnected` links)
    , twixtNextPlayer = next
    }
  links = initLinks server
  structures = initStructures links server
  t1jFor p = initT1J p state
  pegsFor p = PointSet.fromList (serverPegsFor p server)
  swamps = PointSet.fromList (serverSwamps server)
  next = turnNext $ serverTurn server

data FullState = S
  { links :: Links
  , state :: TwixtState
  , pegsFor :: PlayerColor -> PointSet
  , swamps :: PointSet
  , t1jFor :: PlayerColor -> T1J
  , structures :: Structures
  , server :: ServerState
  , next :: PlayerColor
  }

createDiaWindow :: IO (IO [Diagram B], [Diagram B] -> IO ())
createDiaWindow = do
  currentOverlayRef <- newIORef mempty

  board <- Gtk.postGUISync $ do
    board <- Gtk.drawingAreaNew
    let draw = do
          overlay <- readIORef currentOverlayRef
          True <$ defaultRender board (mconcat overlay <> twixtGrid <> guideLinesDia)
    void $ board `Gtk.onExpose` \_ -> draw
    void $ board `Gtk.onConfigure` \_ -> draw

    window <- Gtk.windowNew
    Gtk.set window
      [ Gtk.windowDefaultWidth := 200, Gtk.windowDefaultHeight := 200
      , Gtk.containerChild := board
      ]
    Gtk.widgetShowAll window
    return board

  return . (,) (readIORef currentOverlayRef) $ \newDia -> do
    newDia `seq` writeIORef currentOverlayRef newDia
    Gtk.postGUISync $ Gtk.widgetQueueDraw board

setReplay :: String -> IO ()
setReplay file = loadReplay twixt file >>= writeStore replayStore

setTurn :: Int8 -> IO ()
setTurn t = writeStore turnStore t >> showTurn t

nextTurn :: IO ()
nextTurn = readStoreDef turnStore (return (-1)) >>= setTurn . (+1)

prevTurn :: IO ()
prevTurn = readStoreDef turnStore (return 1) >>= setTurn . pred

actionsStore :: Store (IO [Diagram B], [Diagram B] -> IO ())
actionsStore = Store 0

guiThreadStore :: Store ThreadId
guiThreadStore = Store 1

replayStore :: Store (Replay ServerState Move GameUpdate)
replayStore = Store 2

turnStore :: Store Int8
turnStore = Store 3

storeExists :: Store a -> IO Bool
storeExists (Store i) = isJust <$> lookupStore i

readStoreDef :: Store a -> IO a -> IO a
readStoreDef (Store i) a = lookupStore i >>= maybe a readStore

actions :: (IO [Diagram B], [Diagram B] -> IO ())
actions = unsafePerformIO $ readStoreDef actionsStore $ do
  forkIO (Gtk.initGUI >> Gtk.mainGUI) >>= writeStore guiThreadStore
  createDiaWindow >>= writeStore actionsStore >> readStore actionsStore
{-# NOINLINE actions #-}

main :: IO ()
main = return ()
