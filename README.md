# Twixt AI player

[![wercker status](https://app.wercker.com/status/283a727eeb189ee876fbb923b47b396d/m/master "wercker status")](https://app.wercker.com/project/bykey/283a727eeb189ee876fbb923b47b396d)

This is an AI player for the game Twixt, written for the SoftwareChallenge 2016 competition.

## Naming

For naming, see http://twixt.wikifoundry.com/page/Naming+Board+Elements+and+Peg+Structures.
* todo: adapt current naming to that guide

## Ideas

### Board representation

* Store metadata on connected structures:
  * End points
  * Distance from the edges
  * ...
* Each position on the board belongs to exactly one connected structure

### Tactics

* Implement basic patterns
  * 4-0 / 4-1 for defense
  * 3-1 / 5-2 / 4-2 for attack
  * Defense to basic patterns (cut positions)
* Implement conditions for patterns. For example, we should not use the
  4-0 pattern if there is already an enemy peg on 4-1 defense position

### Strategy

### T1j-AI 

* The following description assumes that the computer is playing east-west
* Fixed start move: one of (2,10) (3,4) (3,5) (4,3) (5,3) (6,3) chosen randomly and randomly mirrored
* 2nd computer move: (also applies for 1st computer move if computer doesn't begin the game)
  * Always in same column if the column of previous move if it is more than 6 away from edges
  * Otherwise, take the column that is 2/3 (random) units closer to center
  * If the column is still not more than 4 units away from the edge, add one
  * If the 2nd computer move would be less than 2x2 units away from 1st, play a 5-0 instead
  * If the 2nd computer move would be less than 2x2 units away from 1st enemy move, fallback to normal search
* 3rd computer move: (blocking move)
  * Find a counter move to one of the enemy moves
  * Counter move must be more than 3 units (sum of x and y diff) away from all other pegs
  * Best move is the move with greatest minimum distance from all other pegs
  * Calculation of counter move:
    * Never generate a counter move for pegs less than 4 units away from edges
    * Generate 4-0 blocking the enemy, blocking the longer side of the enemy peg
    * Generate 4-1 toward center if enemy peg is more than 3 columns away from center
* Main game:
  * Alpha-beta search. Iterative deepening, start with ply 3 search, skip ply 4 search (no idea why)
  * Zobrist hashing to cache best evaluation of situations (disabled in T1j by defauklt)
  * Selective move generation through patterns: defensive and offensive patterns
    * defensive patterns are tried for endpoints of opponent's chains
    * offensive patterns are tried for endpoints of own chains
    * "critical" points have direction assigned: only patterns in the specified directions are tried
    * last move position is always a critical point
  * Evaluation function (described only for the top to bottom player):
    * Initialize each point on row 0 with the value 0
    * Traverse the rows of the board. For each point in the current row, calculate the "father"
      of the point as follows:
      * the father is "blocked" if no connection from this field going up is possible.
      * the father is "point blocked" if there is an enemy peg directly above the current point,
        so in the same column but the previous row.
      * if we have a peg directly above the current point (in the previous row same column), that
        peg is the father of the current point.
      * if we could connect this point to any of our own pegs above it, that peg is the father
        (narrow connections are checked first) (overrides all previous checks)
      * otherwise, the father is the father of the point in the same column of the previous row
    * For each point, we define a function dist(point) that is computed as follows from the father
      of the point:
      * if the father is "blocked" or "point blocked", dist(point) is very large
      * if the father has row=0 (father is a point on the baseline), then dist(point) is
        the row-coordinate of the point
      * otherwise, dist(point) is value of the father of point plus the row difference between the
        point and its father
      * if the column difference between point and its father is 1 or 2, then 1 resp. 3 is added
        before returning the value
    * Calculate the value of a point using the following algorithm:
      * initialize value with dist(point), initialize relevantPoint with father of point
      * if point's father is "point blocked", check if any of the 4 connections to the top are
        possible (not blocked), preferring the connections that go wide (more to the side than top)
        If that is the case, set value to dist(connection target) and relevant point to father of
        the connection target
      * if the relevant point lies on the baseline (row = 0), check that the column distance of
        point and relevant point is <= 2 if we don't have the next move and also check for
        ladders
      * if the above condition holds and the calculated value is better than the previous value of
        the point, store the relevant point for this point and set the value of all points in the
        same peg group as this point to the calculated value
    * Now, the final score is obtained by computing the minimum of the following calculation 
      performed for each point on the bottom row (bottom baseline):
      * if the point has an own peg, take the value of the point
      * otherwise, compute dist(point)
      * TODO: some complications around ladder checks and computing critical points here
     
## Twixt links

* [Commented games from the 13th Little Golem Championship](http://www.trmph.com/hexwiki/Games_from_the_13th_Little_Golem_TwixtPP_Championship.html)
* [Commented games from the 14th Little Golem Championship](http://www.trmph.com/hexwiki/Games_from_the_14th_Little_Golem_TwixtPP_Championship.html)
* [Documentation of T1j-AI](http://www.johannes-schwagereit.de/twixtProgramming/index.html)
* [General TwixT information](http://twixt.wikifoundry.com/)
