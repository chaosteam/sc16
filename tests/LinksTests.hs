{-# LANGUAGE TemplateHaskell #-}
module LinksTests where

import Control.Lens hiding (elements)
import Control.Monad
import Data.List
import Data.Maybe
import SC.Types
import Test.Tasty
import Test.Tasty.TH
import Test.Tasty.QuickCheck

import Coord
import Links
import Game
import Protocol

import ReferenceServer

-- | Returns all the links that would block the given link.
linkBlockedBy :: Link -> [Link]
linkBlockedBy conn =
  [ Link a aToB | a <- allPoints, aToB <- allArrows
  , let b = applyArrow aToB a, b^.row > a^.row, pointValid b
  , let board = unsafeAddLink (Link a aToB) emptyLinks, not $ linkable conn board
  ]

prop_link_blocked_symmetric :: Link -> Bool
prop_link_blocked_symmetric conn =
  process (linkBlockedBy conn) == process (linkBlocks conn)
 where
  process = nub . sort . map linkEnds

prop_update_matches_server_state :: ServerState -> Property
prop_update_matches_server_state server = forAll (serverStateMove server) $ \move ->
  let
    pegs = serverPegsFor (turnNext (serverTurn server)) server
    c0 = initLinks server
    c1 = updateLinks (`elem` pegs) move c0
    c1' = initLinks (updateServerState move server)
  in c1' == c1

tests :: TestTree
tests = $(testGroupGenerator)
