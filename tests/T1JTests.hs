{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
module T1JTests (tests) where

import Control.Lens hiding (elements)
import Control.Monad
import Data.List
import Test.Tasty
import Test.Tasty.TH
import Test.Tasty.QuickCheck
import SC.Types

import Links
import Coord
import Protocol
import T1J
import Game

import qualified PointSet

import ReferenceServer

initState :: ServerState -> TwixtState
initState server@ServerState{..} = TwixtState
  { twixtHasPeg = \player point -> PointSet.member point (if player == PlayerRed then r else b)
  , twixtHasSwamp = (`PointSet.member` swamps)
  , twixtHasLink = hasLink ?? links
  , twixtLinkable = linkable ?? links
  , twixtConnected = findConnected ?? links
  , twixtNextPlayer = turnNext serverTurn
  }
 where
  r = PointSet.fromList serverPegsRed
  b = PointSet.fromList serverPegsBlue
  links = initLinks server
  swamps = PointSet.fromList serverSwamps

prop_update_matches_server_state :: Blind ServerState -> Property
prop_update_matches_server_state (Blind server) =
 forAll (serverStateMove server) $ \move ->
 forAll (elements [PlayerBlue, PlayerRed]) $ \player ->
  let
    next = turnNext $ serverTurn server
    server' = updateServerState move server
    next' = turnNext $ serverTurn server'
    s0 = initState server
    s1 = initState server'
    t0 = initT1J player s0
    t1 = initT1J player s1
    t1' = updateT1J player (s1 { twixtNextPlayer = next }) move t0
  in
    counterexample ("next player: " ++ show next) $
    counterexample ("server state: \n" ++ showServerStateASCII server) $
    counterexample ("t1j player: " ++ show player) $
    counterexample ("t0: " ++ showT1JASCII t0) $
    counterexample ("t1: " ++ showT1JASCII t1) $ counterexample ("t1': " ++ showT1JASCII t1') $
    property $ t1 == t1'

tests :: TestTree
tests = $(testGroupGenerator)
