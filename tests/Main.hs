module Main where

import Test.Tasty

import qualified LinksTests
import qualified StructuresTests
import qualified T1JTests

main :: IO ()
main = defaultMain $ testGroup "sc16"
  [ LinksTests.tests
  , StructuresTests.tests
  , T1JTests.tests
  ]
