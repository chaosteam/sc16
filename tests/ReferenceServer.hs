{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
-- | This module implements updating of the server state, so it can be used as a reference for tests.
module ReferenceServer
  ( updateServerState, serverStateStart, serverStatePlayer, serverStateMove
  , showServerStateASCII
  ) where

import Control.Monad (replicateM)
import Control.Lens ((^.), view)
import Data.List
import Data.Foldable (fold)
import SC.Types
import Test.QuickCheck

import qualified Data.Map as Map
import qualified Data.Set as Set

import Coord
import Game
import Protocol
import Score

import qualified PointSet

-- | Given a server state, generate an arbitrary move that is a connecting move with about 50%
-- chance.
serverStateMove :: ServerState -> Gen Move
serverStateMove server = fmap Move $ do
  conn <- elements [False, True]
  elements $ if conn && not (null connecting) then connecting else allValid
 where
  next = turnNext (serverTurn server)
  pegs = serverPegsBlue server ++ serverPegsRed server ++ serverSwamps server
  valid p = not (p `elem` pegs) && pointValidForPlayer next p
  allValid = filter valid allPoints
  connecting = filter valid . concatMap (map linkTarget . allPointLinks) $
    serverPegsFor next server

-- | Generate an arbitrary server state where no moves have been made yet.
serverStateStart :: Gen ServerState
serverStateStart = do
  swamp3 <- swamp 3 <$> elements (filter valid allPoints)
  swamp2 <- concatMap (swamp 2) <$> replicateM 2 (elements (filter valid allPoints))
  swamp1 <- elements $ filter valid allPoints
  pure $ ServerState
    { serverTurn = 0
    , serverScoreRed = 0
    , serverScoreBlue = 0
    , serverPegsRed = []
    , serverPegsBlue = []
    , serverSwamps = filter valid $ swamp3 ++ swamp2 ++ [swamp1]
    , serverLinks = []
    }
 where
  valid p = p^.row /= 0 && p^.col /= 0
  swamp n p = [Point x y | x <- [(p^.row)..(p^.row + n - 1)], y <- [(p^.col)..(p^.col + n - 1)]]

-- | Generate an arbitrary server state where the given player has to make a move.
serverStatePlayer :: PlayerColor -> Gen ServerState
serverStatePlayer player = do
  s <- serverStateStart
  n <- elements [0..30]
  iterate (>>= go) (return s) !! (2 * n + add)
 where
  add = if player == PlayerBlue then 1 else 0
  go s = flip updateServerState s <$> serverStateMove s

instance Arbitrary ServerState where
  arbitrary = elements [PlayerRed, PlayerBlue] >>= serverStatePlayer

-- | Update the server state. This function is not optimized for speed at all, but rather for
-- correctness.
updateServerState :: Move -> ServerState -> ServerState
updateServerState (Move point) server@ServerState{..} = ServerState
  { serverTurn = serverTurn + 1
  , serverScoreRed = score PlayerRed
  , serverScoreBlue = score PlayerBlue
  , serverPegsRed = pegs PlayerRed
  , serverPegsBlue = pegs PlayerBlue
  , serverSwamps = serverSwamps
  , serverLinks = links'
  }
 where
  next = turnNext serverTurn
  links' = concat
    [ possibleLinks next point (serverPegsFor next server) serverLinks
    , serverLinks
    ]
  score p = max (serverScoreFor p server) $ if p == next
    then calculateScore p point links'
    else 0
  pegs p = (if p == next then (point :) else id) (serverPegsFor p server)

calculateScore :: PlayerColor -> Point -> [(PlayerColor, Point, Point)] -> Score
calculateScore player point links = fromIntegral $ maximum reachable - minimum reachable
 where
  f = view $ axisCoord (playerAxis player)
  reachable = map f . Set.toList $ go mempty point
  linkable = Map.fromListWith (++) [ (source, [target]) | (_, source, target) <- links ]
  go acc p
    | p `Set.member` acc = acc
    | otherwise = foldl' go (Set.insert p acc) (fold $ Map.lookup p linkable)

possibleLinks
  :: PlayerColor -> Point -> [Point] -> [(PlayerColor, Point, Point)]
  -> [(PlayerColor, Point, Point)]
possibleLinks player point pegs links =
  [ (player, point, target)
  | conn <- allPointLinks point
  , let target = linkTarget conn, target `elem` pegs
  , not $ blocked conn
  ]
 where
  conns = map (\(_, a, b) -> (a,b)) links
  blocked conn = any (`elem` conns) blocks
   where
    blocks = [(linkSource l, linkTarget l) | b <- linkBlocks conn, l <- [b, reverseLink b]]

showServerStateASCII :: ServerState -> String
showServerStateASCII server = unlines
  [ "* board"
  , PointSet.drawASCII
      [('r', PointSet.fromList (serverPegsRed server))
      ,('b', PointSet.fromList (serverPegsBlue server))
      ,('s', PointSet.fromList (serverSwamps server))
      ]
  , "* links:"
  , intercalate "\n" ["    " ++ show a ++ " -> " ++ show b | (_,a,b) <- sort (serverLinks server)]
  ]
