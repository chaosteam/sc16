{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DataKinds #-}
module StructuresTests (tests) where

import Control.Lens hiding (elements)
import Control.Monad
import Data.List
import Test.Tasty
import Test.Tasty.TH
import Test.Tasty.QuickCheck
import SC.Types

import Links
import Coord
import Protocol
import Structures
import Game

import ReferenceServer

prop_update_matches_server_state :: Blind ServerState -> Property
prop_update_matches_server_state (Blind server) = forAll (serverStateMove server) $ \move ->
  let
    next = turnNext $ serverTurn server
    c0 = initLinks server
    c1 = initLinks (updateServerState move server)
    s0 = initStructures c0 server
    s1 = updateStructures next c1 move s0
    s1' = initStructures c1 (updateServerState move server)
  in
    counterexample ("player: " ++ show next) $
    counterexample ("server state: \n" ++ showServerStateASCII server) $
    counterexample ("differences: \n" ++ diffStructures s1 s1') $
    s1 == s1'

tests :: TestTree
tests = $(testGroupGenerator)
