module Main where

import Control.Lens
import SC.GameDefinition
import SC.Replay
import SC.Strategy
import SC.Types
import System.Environment

import Game
import Protocol

import qualified Strategy.Monkey as Monkey
import qualified Strategy.Brute as Brute
import qualified Strategy.T1J as T1J

type Situation = (TurnState Identity Proxy ServerState Move GameUpdate)

strategies :: String -> StrategyCreator ServerState Move GameUpdate
strategies "brute" = Brute.strategy
strategies "t1j" = T1J.strategy
strategies _ = Monkey.strategy

statStrategy :: Int -> StrategyCreator ServerState Move GameUpdate -> Situation -> String
statStrategy maxDepth strategyF state = view _1 $ moves (strategyF conf GameUpdate gstate) !! maxDepth
 where
  gstate = runIdentity . initState $ state
  conf = GameConfiguration { selfColor = turnNext $ serverTurn gstate, startColor = PlayerRed }

turnsWithInitial :: Replay init move state -> [TurnState Identity Proxy init move state]
turnsWithInitial replay = initialSituation replay : map proxyLastMove (turns replay) where
  proxyLastMove m = m { lastMove = Proxy }

loadSituation :: FilePath -> Int -> IO Situation
loadSituation file turnId = do
  replay <- loadReplay twixt file
  return $ turnsWithInitial replay !! turnId

main :: IO ()
main = do
  [strat, file, turn, depth] <- getArgs
  situation <- loadSituation file (read turn)
  putStrLn $ statStrategy (read depth) (strategies strat) situation
